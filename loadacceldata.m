function [manifestlist] = loadacceldata(settings,filesfound)
%LOADACCELDATA - loads accelerometry data from csv
%
% This function loads the accelerometry data from the csv files and
% organizes the data by subject/visit, writing each subject to it's own mat
% file
%
% settings: settings for YAAAT.m
% filesfound: table containing list of csv files to load
%
% Created by Andrew Van, 05/22/2017

% create data folder if not exists
if ~exist([settings.directory filesep 'data'],'dir')
    mkdir([settings.directory filesep 'data']);
end

% create plot folder if not exists
if ~exist([settings.directory filesep 'plots'],'dir')
    mkdir([settings.directory filesep 'plots']);
end

% Sort the manifest by ascending subject name
filesfound = sortrows(filesfound,'sub_name','ascend');

% Return unique subjects from the manifest
unique_subs = unique(filesfound.sub_name);

% Load/Create dataset object
if exist([settings.directory filesep 'data' filesep 'manifest.mat'],'file')
    % load manifest
    load([settings.directory filesep 'data' filesep 'manifest.mat']);
else
    manifestlist = manifest;
end

% loop through subjects
for i = 1:length(unique_subs)
    % get subject visits
    visit_table = filesfound(strcmp(filesfound.sub_name,unique_subs{i}),:);

    % check if subject data already exists
    if exist([settings.directory filesep 'data' filesep unique_subs{i} '.mat'],'file') && ~settings.force_process_all
        % load saved data
        load([settings.directory filesep 'data' filesep unique_subs{i} '.mat']);
        % get already processed visits
        processed_visits = unique(sub.data.V_num);
        % assign sub data to data
        data = sub.data;
    else
        % create empty table for data
        data = table([],[],[],{},{},'VariableNames',{'V_num','time_left','time_right','raw_data_left','raw_data_right'});
        processed_visits = [];
    end

    % get unique visits
    unique_visits = unique(visit_table.V_num);

    % only do non-processed visits
    unique_visits = unique_visits(~ismember(unique_visits,processed_visits));

    % loop through visits
    for j = 1:length(unique_visits)
        disp(['Adding Subject ' unique_subs{i} ', Visit #' num2str(unique_visits(j))]);

        % get visit
        sub_visit = visit_table(visit_table.V_num==unique_visits(j),:);

        % get left hand
        left = sub_visit(sub_visit.LoR==76,:);
        disp(['Parsing ' left.filename{1}]);

        % read in left hand data
        fileID_left = fopen([settings.directory filesep 'csv' filesep left.filename{1}],'r');
        % read start time
        time_buffer1 = textscan(fileID_left, '%*s%*s%s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%[^\n\r]',...
            1, 'Delimiter', ' ', 'HeaderLines', 2, 'ReturnOnError', false);
        % read start date (text scan continues on from last line read since
        % same formatspec)
        time_buffer2 = textscan(fileID_left, '%*s%*s%s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%[^\n\r]',...
            1, 'Delimiter', ' ', 'HeaderLines', 1, 'ReturnOnError', false);
        % read accel data (text scan continues on from last line read since
        % same formatspec)
        buffer = textscan(fileID_left, '%f%f%f%[^\n\r]', 'Delimiter', ',', 'EmptyValue',...
            NaN, 'HeaderLines', 7, 'ReturnOnError', false);
        % close file
        fclose(fileID_left);

        % move data from buffer to variables
        raw_data_left = [buffer{1:end-1}];
        time_buffer1 = [time_buffer1{1:end-1}];
        % use serial number format for date
        time_left = datenum([time_buffer2{1:end-1}],'mm/dd/yyyy') +...
            datenum(['0000-01-00',' ',time_buffer1{1}],'yyyy-mm-dd HH:MM:SS');

        % get right hand
        right = sub_visit(sub_visit.LoR==82,:);
        disp(['Parsing ' right.filename{1}]);

        % read in right hand data
        fileID_right = fopen([settings.directory filesep 'csv' filesep right.filename{1}],'r');
        % read start time
        time_buffer1 = textscan(fileID_right, '%*s%*s%s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%[^\n\r]',...
            1, 'Delimiter', ' ', 'HeaderLines', 2, 'ReturnOnError', false);
        % read start date (text scan continues on from last line read since same formatspec
        time_buffer2 = textscan(fileID_right, '%*s%*s%s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%[^\n\r]',...
            1, 'Delimiter', ' ', 'HeaderLines', 1, 'ReturnOnError', false);
        % read accel data
        buffer = textscan(fileID_right, '%f%f%f%[^\n\r]', 'Delimiter', ',', 'EmptyValue',...
            NaN, 'HeaderLines', 7, 'ReturnOnError', false);
        % close file
        fclose(fileID_right);

        % move data from buffers to variables
        raw_data_right = [buffer{1:end-1}];
        time_buffer1 = [time_buffer1{1:end-1}];
        % use serial number format for date
        time_right = datenum([time_buffer2{1:end-1}],'mm/dd/yyyy') +...
            datenum(['0000-01-00',' ',time_buffer1{1}],'yyyy-mm-dd HH:MM:SS');

        % add visit data to subject table
        data = [data; table(unique_visits(j),time_left,time_right,{raw_data_left},{raw_data_right},...
            'VariableNames',{'V_num','time_left','time_right','raw_data_left','raw_data_right'})]; %#ok<AGROW>
    end

    % only save subject mat if unique visits processed
    if ~isempty(unique_visits)
        % load the age data (if exists)
        if exist([settings.directory filesep 'age' filesep 'age.csv'],'file')
            age_data = importagefile([settings.directory filesep 'age' filesep 'age.csv']);

            % get the age of the subject; set to -1 if no data
            age_idx = strcmp(unique_subs{i},age_data.IDNumber);
            if sum(age_idx) ~= 0
                age = age_data.Age(age_idx);
                if age == 999
                    age = -1;
                end
            else % age not found
                age = -1;
            end
        else % age not found
            age = -1;
        end

        % sort data table by ascending visit
        data = sortrows(data,'V_num','ascend');

        % construct subject object
        sub = subject(unique_subs{i},age,visit_table.domhand{1},data);

        % add/re-add subject to subject list
        manifestlist.addsubject(unique_subs(i),{data.V_num});

        % save info for subject
        save([settings.directory filesep 'data' filesep sub.name '.mat'],'sub');
    end
end

% save manifestlist
save([settings.directory filesep 'data' filesep 'manifest.mat'],'manifestlist');

end
