function [] = YAAAT(directory,sr,fpa,cs,bs,at)
try
    % Load settings
    settings.directory = directory;
    settings.sample_rate = sr;
    settings.force_process_all = fpa;
    settings.cut_size = cs;
    settings.block_size = bs;
    settings.activity_threshold = at;

    % FOR DEBUGGING
    %addpath('./classdef');
    %clc;
    %clear;
    %close all;
    %settings.directory = './test_data';
    %settings.sample_rate = 1;
    %settings.force_process_all = true;
    %settings.cut_size = 30;
    %settings.block_size = 30;
    %settings.activity_threshold = 10;

    if settings.force_process_all
        disp('Force Processing is Enabled. Processing all files...');
    end

    % Updates manifest and process files
    updatemanifest(settings);
    generateanalysis(settings,'block');
    generateanalysis(settings,'visit');
    generateanalysis(settings,'subject');
catch exception
    % Get/display exception text
    msgText = getReport(exception);
    disp(msgText);
end
end
