function [manifestlist] = updatemanifest(settings)
%UPDATEMANIFEST - updates the manifest file of the current dataset
%
% settings: settings from YAAAT.m
% datasetlist
%
% Created by Andrew Van, 05/18/2017

% get files to process in directory
files = dir([settings.directory filesep 'csv' filesep '*.csv']);

% sort files by name
[~, idx] = sort({files.name});
files = files(idx);
disp([num2str(length(files)) ' files found.'])

% store names of files in table
filesfound = table(...
    cell(length(files),1),...
    cell(length(files),1),...
    zeros(length(files),1),...
    zeros(length(files),1),...
    cell(length(files),1),...
    'VariableNames',{...
        'filename',...
        'sub_name',...
        'V_num',...
        'LoR',...
        'domhand'...
    }...
);

for i=1:length(files)
	% store the filenames in a cell
	filesfound.filename{i} = files(i).name;
    
	% parse the name string
	parsed = strfind(strrep(filesfound.filename{i}, '.csv', ''), '_');
	
	% Get the subject name
	filesfound.sub_name{i} = filesfound.filename{i}(1:parsed(1)-1);
    
	% get the number in V# structure
	filesfound.V_num(i) = str2double(filesfound.filename{i}(parsed(1)+2:parsed(2)-1));
	
	% get the L(76) or R(82) tag
	filesfound.LoR(i) = uint8(filesfound.filename{i}(parsed(3)+1:parsed(4)-1));
    
	% get the dominant hand
	filesfound.domhand{i} = filesfound.filename{i}(parsed(4)+1:parsed(5)-1);
end

% load data files from filesfound
manifestlist = loadacceldata(settings,filesfound);

end