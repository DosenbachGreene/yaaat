function [] = generateanalysis(settings,type)
%GENERATEANALYSIS - generates the various statistics for the dataset
%
% settings: settings from YAAAT.m
% type: type of calculation to do; options are block, visit, subject, or
% dataset
%
% Andrew Van, 05/23/17, vanandrew@wustl.edu
%#ok<*NODEF>

% hack to make figures actually hide
set(0, 'DefaultFigureVisible', 'off'); F = figure; set(0, 'DefaultFigureVisible', 'off'); close(F);

% load manifest file
assert(logical(exist([settings.directory filesep 'data' filesep 'manifest.mat'],'file')),'Dataset list not found.');
load([settings.directory filesep 'data' filesep 'manifest.mat']);

% process by block
if strcmp(type,'block')
    % check if block data already exists
    if exist([settings.directory filesep 'data' filesep 'blockdata.mat'],'file') && ~settings.force_process_all
        load([settings.directory filesep 'data' filesep 'blockdata.mat']);
        % compare dataset lists
        list = manifestlist.comparelists(data.list);

        % clear subject in list for processing/reprocessing
        data.clearsubject(list.Name);
    else
        % create a new blockdata object
        data = blockdata;
        list = manifestlist.subjectlist;
    end

    % add subjects in list to block data
    data.addsubjects(list,settings);

    % save block data to file
    save([settings.directory filesep 'data' filesep 'blockdata.mat'],'data');

    % write to csv
    data.writecsvfile(settings,'blocked');
end

% process by visit
if strcmp(type,'visit')
    % check if visit data already exists
    if exist([settings.directory filesep 'data' filesep 'visitdata.mat'],'file') && ~settings.force_process_all
        load([settings.directory filesep 'data' filesep 'visitdata.mat']);
        % compare dataset lists
        list = manifestlist.comparelists(data.list);

        % clear subject in list for processing/reprocessing
        data.clearsubject(list.Name);
    else
        % create a new blockdata object
        data = visitdata;
        list = manifestlist.subjectlist;
    end

    % add subjects in list to visit data
    data.addsubjects(list,settings);

    % save visit data to file
    save([settings.directory filesep 'data' filesep 'visitdata.mat'],'data');

    % generate sleep plots
    data.sleepbyvisit(settings);
    data.sleepbyage(settings);

    % write to csv
    data.writecsvfile(settings,'visit');
end

% process by subject
if strcmp(type,'subject')
    % check if visit data already exists
    if exist([settings.directory filesep 'data' filesep 'subjectdata.mat'],'file') && ~settings.force_process_all
        load([settings.directory filesep 'data' filesep 'subjectdata.mat']);
        % compare dataset lists
        list = manifestlist.comparelists(data.list);

        % clear subject in list for processing/reprocessing
        data.clearsubject(list.Name);
    else
        % create a new blockdata object
        data = subjectdata;
        list = manifestlist.subjectlist;
    end

    % add subjects in list to visit data
    data.addsubjects(list,settings);

    % save visit data to file
    save([settings.directory filesep 'data' filesep 'subjectdata.mat'],'data');

    % sort by age
    data.sortbyage(settings);
    data.sortbyage2(settings);

    % plot density figures
    data.plotavgdensitysubject(list.Name,settings);

    % write to csv
    data.writecsvfile(settings,'subject');
end

end
