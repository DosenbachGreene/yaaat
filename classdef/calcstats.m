function [stats] = calcstats(data, activity_threshold, sample_rate, domhand)
%CALCSTATS calculates the statistics for the accelerometry data input
%
% inputs:
%
% data: contains activity counts in left/right UE; stored
% data.left/data.right
%
% activity_threshold: the threshold for detecting activity; set in RUN.m
%
% samples: contains sampling rate
%
% domhand: string containing the dominant UE; either 'RH' or 'LH'
%
% outputs:
%
% stats: structure containing calculated statisitcs from input data; too
% many to list here...; use debug mode to examine the calculated stats
%
%   Andrew Van, "05/23/2017", vanandrew@wustl.edu

% define samples
samples.samples_per_hour = 3600*sample_rate;
samples.samples_per_minute = samples.samples_per_hour / 60;

% Get the LH counts above the threshold
LCount = find(data.left > activity_threshold);
LUseDenominator = length(data.left);
stats.LUseCount = length(LCount);
stats.weightedLUseCount = sum(LCount);

% Convert to other variabkes (LH)
stats.LWearingTime = LUseDenominator/samples.samples_per_hour;
LMinuteCount = stats.LUseCount/samples.samples_per_minute;
stats.LHourCount = LMinuteCount/60;
stats.LPercentUse = (stats.LUseCount/LUseDenominator) * 100;

% Get the RH counts above the threshold
RCount = find(data.right > activity_threshold);
RUseDenominator = length(data.right);
stats.RUseCount = length(RCount);
stats.weightedRUseCount = sum(RCount);

% Convert to other variables (RH)
stats.RWearingTime = RUseDenominator/samples.samples_per_hour;
RMinuteCount = stats.RUseCount/samples.samples_per_minute;
stats.RHourCount = RMinuteCount/60;
stats.RPercentUse = (stats.RUseCount/RUseDenominator) * 100;

% Get the dominant side of the current subject and the Use-Ratio
if strcmp(domhand, 'LH')
    stats.UseRatio = (stats.RPercentUse/stats.LPercentUse);
    stats.WeightedUseRatio = (stats.weightedRUseCount/stats.weightedLUseCount);
elseif strcmp(domhand, 'RH')
    stats.UseRatio = (stats.LPercentUse/stats.RPercentUse);
    stats.WeightedUseRatio = (stats.weightedLUseCount/stats.weightedRUseCount);
end

% Set Use Ratio to 0 if undefined
if isnan(stats.UseRatio)
    stats.UseRatio = 0;
    stats.WeightedUseRatio = 0;
end

% Filter out no-data points
nodata_idx_L = (data.left == -1);
nodata_L = data.left.*nodata_idx_L;
nodata_idx_R = (data.right == -1);
nodata_R = data.right.*nodata_idx_R;
if strcmp(domhand, 'LH')
    stats.Dom_UE_nodata = nodata_L;
    stats.Dom_UE_nodata(nodata_L==0) = [];
    stats.ND_UE_nodata = nodata_R;
    stats.ND_UE_nodata(nodata_R==0) = [];
elseif strcmp(domhand, 'RH')
    stats.Dom_UE_nodata = nodata_R;
    stats.Dom_UE_nodata(nodata_R==0) = [];
    stats.ND_UE_nodata = nodata_L;
    stats.ND_UE_nodata(nodata_L==0) = [];
end
if isempty(stats.Dom_UE_nodata)
    stats.Dom_UE_nodata = [];
end
if isempty(stats.ND_UE_nodata)
    stats.ND_UE_nodata = [];
end

% Filter out data points below activity threshold
Li = data.left > activity_threshold;
L_data = data.left.*Li;
Ri = data.right > activity_threshold;
R_data = data.right.*Ri;
if strcmp(domhand, 'LH')
    stats.Dom_UE = L_data;
    stats.ND_UE = R_data;
elseif strcmp(domhand, 'RH')
    stats.Dom_UE = R_data;
    stats.ND_UE = L_data;
end

% Get the recording period
stats.Recording_Period = (length(stats.Dom_UE))/samples.samples_per_hour;

% Calculate number of no movement points
d = (stats.Dom_UE==0 & stats.ND_UE==0);
stats.No_Movement = sum(d)/samples.samples_per_hour;
if ~isempty(d) % only get rid if any are found
    stats.Dom_UE(d) = [];
    stats.ND_UE(d) = [];
end

% Find the time spent in unilateral Dom
a = find(stats.Dom_UE>0 & stats.ND_UE==0);
stats.Uni_Dom_UE = (stats.Dom_UE(a));
if isempty(a)
    stats.Uni_Dom_UE = [];
end

% Find the time spent in unilateral ND
b = find(stats.ND_UE>0 & stats.Dom_UE==0);
stats.Uni_ND_UE = (stats.ND_UE(b));
if isempty(b)
    stats.Uni_ND_UE = [];
end

% Find time spend in bilateral movement
c = find(stats.ND_UE==0 | stats.Dom_UE==0); % Areas where they are unilateral moving
stats.Dom_UE_bi = stats.Dom_UE;
stats.ND_UE_bi = stats.ND_UE;
if ~isempty(c)
    % delete areas unilateral movement
    stats.Dom_UE_bi(c) = [];
    stats.ND_UE_bi(c) = [];

    if isempty(stats.Dom_UE_bi) || isempty(stats.ND_UE_bi)
        % Calculate bilateral magnitude
        stats.magnitude = 0; % set to zero b/c no data

        % Calculate the ratio between ND and Dom
        stats.ratio = 0; % set to zero b/c no data
    else
        % Calculate bilateral magnitude
        stats.magnitude = stats.Dom_UE_bi+stats.ND_UE_bi;

        % Calculate the ratio between ND and Dom
        stats.ratio = log(stats.ND_UE_bi./stats.Dom_UE_bi);
    end
else % areas of unilateral movement not found
    if isempty(stats.Dom_UE_bi) || isempty(stats.ND_UE_bi)
        % Calculate bilateral magnitude
        stats.magnitude = 0; % set to zero b/c no data

        % Calculate the ratio between ND and Dom
        stats.ratio = 0; % set to zero b/c no data
    else
        % Calculate bilateral magnitude
        stats.magnitude = stats.Dom_UE+stats.ND_UE;

        % Calculate the ratio between ND and Dom
        stats.ratio = log(stats.ND_UE./stats.Dom_UE);
    end
end
if isempty(stats.Dom_UE_bi)
    stats.Dom_UE_bi = [];
end
if isempty(stats.ND_UE_bi)
    stats.ND_UE_bi = [];
end

% Calculate the overall time moving
stats.Time_Moving = stats.Recording_Period - stats.No_Movement;

% Calculate time spent in simultaneous UE movement, &
% Percent of time spent in simultaneous UE movement, in hours.
e=find(stats.Dom_UE>0 & stats.ND_UE>0);
stats.Time_Simultaneous=(length(e))/samples.samples_per_hour;
stats.Dom_Unilateral=(length(stats.Uni_Dom_UE))/samples.samples_per_hour;
stats.ND_Unilateral=(length(stats.Uni_ND_UE))/samples.samples_per_hour;

% Calculate the unilateral ratio
stats.UniUseRatio = length(stats.Uni_ND_UE)/length(stats.Uni_Dom_UE);
stats.WeightedUniUseRatio = sum(stats.Uni_ND_UE)/sum(stats.Uni_Dom_UE);

% Calculate the bilateral ratio
stats.BiUseRatio = length(stats.ND_UE_bi)/length(stats.Dom_UE_bi);
stats.WeightedBiUseRatio = sum(stats.ND_UE_bi)/sum(stats.Dom_UE_bi);

% Calculate median magnitude/ratio
stats.Median_Magnitude = median(stats.magnitude);
stats.Median_Ratio = median(stats.ratio);

% Calculate %
stats.percentnomov = (stats.No_Movement/stats.Recording_Period)*100;
stats.percenttimemov = (stats.Time_Moving/stats.Recording_Period)*100;
stats.percentsim = (stats.Time_Simultaneous/stats.Recording_Period)*100;
stats.percentDomUni = (stats.Dom_Unilateral/stats.Recording_Period)*100;
stats.percentNDUni = (stats.ND_Unilateral/stats.Recording_Period)*100;

end
