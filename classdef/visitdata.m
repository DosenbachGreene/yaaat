classdef visitdata < datacontainer
    %VISITDATA - class object for holding visit data
    %
    % Andrew Van, 05/25/17, vanandrew@wustl.edu

    properties
    end

    methods
        function addsubjects(obj,list,settings)
            %ADDSUBJECT - add subjects to data container
            %   input:
            %       list: list of subjects to add to container
            %       settings: settings structure (See YAAAT.m)

            % function for converting time to cell
            format_time = @(x) cellstr(datestr(x));

            % loop through each subject
            for i=1:height(list)
                disp(['Processing Visit Data for ', num2str(list.Name{i})]);

                % load subject data
                load([settings.directory filesep 'data' filesep list.Name{i} '.mat']);

                % preprocess subject data
                sub.makesamelength();
                sub.cutout(settings.cut_size,settings.sample_rate);
                sub.calcresultant();
                sub.detectwear();
                sub.detectsleep();

                % process each visit
                for j=1:height(sub.data)
                    % Get the time information for the visit
                    one_sample_in_days = (((1/settings.sample_rate)/60)/60)/24;
                    time_start = format_time(sub.data.time_left(j));
                    time_done = format_time(sub.data.time_left(j) + one_sample_in_days*(length(sub.data.LVMData{j})-1));

                    % format data structure
                    ds.left = sub.data.LVMData{j};
                    ds.right = sub.data.RVMData{j};

                    % Get the stats for each hand
                    stats = calcstats(ds, settings.activity_threshold, settings.sample_rate, sub.hand);

                    % Save visit to table
                    obj.addrow(list.Name(i),sub.age,sub.data.V_num(j),{sub.hand},...
                        time_start,time_done,round(stats.Recording_Period*60),...
                        stats.UseRatio,stats.WeightedUseRatio,...
                        stats.UniUseRatio,stats.BiUseRatio,...
                        stats.WeightedUniUseRatio,stats.WeightedBiUseRatio,...
                        stats.LUseCount, stats.RUseCount,...
                        stats.percentnomov,stats.percenttimemov,stats.percentsim,...
                        stats.percentDomUni,stats.percentNDUni,{stats.magnitude},...
                        {stats.ratio},{stats.Uni_Dom_UE},{stats.Uni_ND_UE},...
                        {stats.Dom_UE_bi},{stats.ND_UE_bi},...
                        {sub.data.wearL{j}},{sub.data.wearR{j}},...
                        {sub.data.s_sleep_l{j}},{sub.data.s_sleep_r{j}},...
                        {sub.data.ck_sleep_l{j}},{sub.data.ck_sleep_r{j}},...
                        {sub.data.g_sleep_l{j}},{sub.data.g_sleep_r{j}});
                end

                % add subject to data list table
                obj.list.addsubject(list.Name(i),{sub.data.V_num});
            end
        end
        function writecsvfile(obj,settings,name)
            %WRITECSVFILE - Writes datalist to human readable CSV format

            % calculate percentage
            percentage = @(x) 100*sum(x)./length(x);

            % construct table for writing
            tw = [table(obj.Name,obj.Age,obj.Visit,obj.Domhand,obj.Start,obj.End,...
                obj.Period,obj.UseRatio,obj.UniUseRatio,obj.BiUseRatio,...
                obj.wUseRatio,obj.wUniUseRatio,obj.wBiUseRatio,...
                obj.LUseCount,obj.RUseCount,obj.PercentNoMov,...
                obj.PercentMov,obj.PercentSim,obj.PercentDomUni,obj.PercentNDUni,'VariableNames',...
                {'Name','Age','Visit','Domhand','Start','End','Period',...
                'UseRatio','UniUseRatio','BiUseRatio','wUseRatio','wUniUseRatio','wBiUseRatio',...
                'LUseCount','RUseCount','PercentNoMov','PercentMov','PercentSim','PercentDomUni','PercentNDUni'}),...
                table(cellfun(@median,obj.BiMagnitude),...
                    cellfun(@max,obj.BiMagnitude),...
                    cellfun(@min,obj.BiMagnitude),...
                    cellfun(@median,obj.BiRatio),...
                    cellfun(@max,obj.BiRatio),...
                    cellfun(@min,obj.BiRatio),...
                    cellfun(percentage,obj.LWear),...
                    cellfun(percentage,obj.RWear),...
                    cellfun(percentage,obj.Sadeh_Sleep_Left),...
                    cellfun(@nnz,obj.Sadeh_Sleep_Left),...
                    cellfun(percentage,obj.Sadeh_Sleep_Right),...
                    cellfun(@nnz,obj.Sadeh_Sleep_Right),...
                    cellfun(percentage,obj.CK_Sleep_Left),...
                    cellfun(@nnz,obj.CK_Sleep_Left),...
                    cellfun(percentage,obj.CK_Sleep_Right),...
                    cellfun(@nnz,obj.CK_Sleep_Right),...
                    cellfun(percentage,obj.Galland_Sleep_Left),...
                    cellfun(@nnz,obj.Galland_Sleep_Left),...
                    cellfun(percentage,obj.Galland_Sleep_Right),...
                    cellfun(@nnz,obj.Galland_Sleep_Right),...
                    'VariableNames',{...
                    'MedianBilateralMagnitude','MaxBilateralMagnitude',...
                    'MinBilateralMagnitude','MedianMagnitudeRatio',...
                    'MaxMagnitudeRatio','MinMagnitudeRatio','LWearPercent',...
                    'RWearPercent','Percent_Sadeh_Sleep_Left','Total_Sadeh_Sleep_Left',...
                    'Percent_Sadeh_Sleep_Right','Total_Sadeh_Sleep_Right',...
                    'Percent_CK_Sleep_Left','Total_CK_Sleep_Left',...
                    'Percent_CK_Sleep_Right','Total_CK_Sleep_Right',...
                    'Percent_Galland_Sleep_Left','Total_Galland_Sleep_Left',...
                    'Percent_Galland_Sleep_Right','Total_Galland_Sleep_Right'}),...
                    table(obj.Wake_Time_L,obj.Wake_Time_R,obj.Sleep_Time_L,obj.Sleep_Time_R,...
                    'VariableNames',{'Wake_Time_L','Wake_Time_R','Sleep_Time_L','Sleep_Time_R'}...
                )];

            % sort table
            tw = sortrows(tw,'Visit','ascend');
            tw = sortrows(tw,'Name','ascend');

             % create results directory if not exist
             if ~exist([settings.directory filesep 'results'],'dir')
                mkdir([settings.directory filesep 'results']);
             end

             % write table to csv
             writetable(tw,[settings.directory filesep 'results' filesep name '.csv']);
        end
        function [] = sleepbyvisit(obj,settings)
           %SLEEPBYVISIT = Generate Sleep plots by visit
           disp('Generating Sleep Plots by Visit');
           % set default figure visibility to off
           set(0, 'DefaultFigureVisible', 'off');

           % create plots directory if not exist
           if ~exist([settings.directory filesep 'plots'],'dir')
              mkdir([settings.directory filesep 'plots']);
           end

           % define sleep plot names
           sp_name{1} = 'sleepbyvisit_sadeh_left';
           sp_name{2} = 'sleepbyvisit_sadeh_right';
           sp_name{3} = 'sleepbyvisit_cole_kripke_left';
           sp_name{4} = 'sleepbyvisit_cole_kripke_right';
           sp_name{5} = 'sleepbyvisit_galland_left';
           sp_name{6} = 'sleepbyvisit_galland_right';

           % create arrays to store sleep plots
           sleep_plot.sl = zeros(numel(obj.Name),1440);
           sleep_plot.sr = zeros(numel(obj.Name),1440);
           sleep_plot.ckl = zeros(numel(obj.Name),1440);
           sleep_plot.ckr = zeros(numel(obj.Name),1440);
           sleep_plot.gl = zeros(numel(obj.Name),1440);
           sleep_plot.gr = zeros(numel(obj.Name),1440);
           obj.Sleep_Time_R = cell(numel(obj.Name),1);
           obj.Sleep_Time_L = cell(numel(obj.Name),1);
           obj.Wake_Time_R = cell(numel(obj.Name),1);
           obj.Wake_Time_L = cell(numel(obj.Name),1);

           % convert to datetime objects
           starttime = datetime(obj.Start);
           endtime = starttime + minutes(cellfun('length',obj.Sadeh_Sleep_Left)-1);

           % get list of sleeps
           sadeh_left = obj.Sadeh_Sleep_Left;
           sadeh_right = obj.Sadeh_Sleep_Right;
           ck_left = obj.CK_Sleep_Left;
           ck_right = obj.CK_Sleep_Right;
           g_left = obj.Galland_Sleep_Left;
           g_right = obj.Galland_Sleep_Right;

           % loop through each visit in the set
           for j=1:length(obj.Name)
               % skip if already plot exists and force process is off
                if exist([settings.directory filesep 'plots' filesep sp_name{1} filesep obj.Name{j} '_',...
                    num2str(obj.Visit(j)) '_' sp_name{1} '.png'],'file') && ~settings.force_process_all
                    continue
                end
                disp(['Subject: ' obj.Name{j} ' Visit: ' num2str(obj.Visit(j))]);

                % Refactor array into a 24 hour span; get the
                % nearest start and end days
                beginning_day = starttime(j) - hours(starttime(j).Hour)...
                    - minutes(starttime(1).Minute) - seconds(starttime(j).Second);
                end_day = endtime(j) - hours(endtime(j).Hour)...
                    - minutes(endtime(j).Minute) - seconds(endtime(j).Second) + caldays(1) - minutes(1);

                % pad the start and ends of the sleep arrays
                sadeh_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    sadeh_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                sadeh_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    sadeh_right{j}; zeros(minutes(end_day-endtime(j)),1)];
                ck_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    ck_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                ck_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    ck_right{j}; zeros(minutes(end_day-endtime(j)),1)];
                g_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    g_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                g_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                    g_right{j}; zeros(minutes(end_day-endtime(j)),1)];

                % find the number of days in-between the start and
                % end days
                time_length = round(days(end_day - beginning_day));

                % loop through the each day; refactor into 24 hr
                % period
                slp = zeros(1440,1);
                srp = zeros(1440,1);
                cklp = zeros(1440,1);
                ckrp = zeros(1440,1);
                glp = zeros(1440,1);
                glr = zeros(1440,1);
                begin = beginning_day;
                begin_element = 1;
                for k=1:time_length
                    % find end of day
                    finish = begin + caldays(1) - minutes(1);

                    % get the number of elements between begin and
                    % finish
                    final_element = minutes(finish - begin) + begin_element;

                    % check if the final_element is longer than sadeh_left; limit to last element
                    if final_element > length(sadeh_left{j})
                        final_element = length(sadeh_left{j});
                    end

                    % find difference between final_element and sleep period
                    dif = length(slp) - length(sadeh_left{j}(begin_element:final_element));

                    % refactor days
                    slp = slp + [sadeh_left{j}(begin_element:final_element); zeros(dif,1)];
                    srp = srp + [sadeh_right{j}(begin_element:final_element); zeros(dif,1)];
                    cklp = cklp + [ck_left{j}(begin_element:final_element); zeros(dif,1)];
                    ckrp = ckrp + [ck_right{j}(begin_element:final_element); zeros(dif,1)];
                    glp = glp + [g_left{j}(begin_element:final_element); zeros(dif,1)];
                    glr = glr + [g_right{j}(begin_element:final_element); zeros(dif,1)];

                    % reset for next day
                    begin = begin + caldays(1);
                    begin_element = final_element + 1;
                end

                % reassign to initial variable
                sleep_plot.sl(j,:) = slp';
                sleep_plot.sr(j,:) = srp';
                sleep_plot.ckl(j,:) = cklp';
                sleep_plot.ckr(j,:) = cklp';
                sleep_plot.gl(j,:) = glp';
                sleep_plot.gr(j,:) = glp';

                % create cell for sleep plot
                sp{1} = sleep_plot.sl(j,:);
                sp{2} = sleep_plot.sr(j,:);
                sp{3} = sleep_plot.ckl(j,:);
                sp{4} = sleep_plot.ckr(j,:);
                sp{5} = sleep_plot.gl(j,:);
                sp{6} = sleep_plot.gr(j,:);

                % go across sadeh sleep to get wake/sleep time start depending on Age
                if (obj.Age(j) >= 0) && (obj.Age(j) <= 3)
                    wake_idx_1 = 241; % 4am
                    wake_idx_2 = 661; % 11am
                    sleep_idx_1 = 1021; % 5pm
                    sleep_idx_2 = 1440; % 11:59pm
                elseif (obj.Age(j) >= 4) && (obj.Age(j) <= 7)
                    wake_idx_1 = 241; % 4am
                    wake_idx_2 = 661; % 11am
                    sleep_idx_1 = 1081; % 6pm
                    sleep_idx_2 = 1500; % 12:59am
                elseif (obj.Age(j) >= 8) && (obj.Age(j) <= 11)
                    wake_idx_1 = 241; % 4am
                    wake_idx_2 = 661; % 11am
                    sleep_idx_1 = 1141; % 7pm
                    sleep_idx_2 = 1560; % 1:59am
                elseif (obj.Age(j) >= 12) && (obj.Age(j) <= 14)
                    wake_idx_1 = 241; % 4am
                    wake_idx_2 = 661; % 11am
                    sleep_idx_1 = 1201; % 8pm
                    sleep_idx_2 = 1620; % 2:59am
                else
                    wake_idx_1 = 241; % 4am
                    wake_idx_2 = 661; % 11am
                    sleep_idx_1 = 1261; % 9pm
                    sleep_idx_2 = 1680; % 3:59am
                end

                % wrap sleep data around at end to get times after midnight
                wrapped_sleep_l = [sp{1}, sp{1}];
                wrapped_sleep_r = [sp{2}, sp{2}];
                % get idxs of sleep changes
                idx_wake_L = find(wrapped_sleep_l(wake_idx_1:wake_idx_2-1) - wrapped_sleep_l(wake_idx_1+1:wake_idx_2));
                idx_wake_R = find(wrapped_sleep_r(wake_idx_1:wake_idx_2-1) - wrapped_sleep_r(wake_idx_1+1:wake_idx_2));
                idx_sleep_L = find(wrapped_sleep_l(sleep_idx_1:sleep_idx_2-1) - wrapped_sleep_l(sleep_idx_1+1:sleep_idx_2));
                idx_sleep_R = find(wrapped_sleep_r(sleep_idx_1:sleep_idx_2-1) - wrapped_sleep_r(sleep_idx_1+1:sleep_idx_2));
                % grab the first idx
                start_of_day = starttime(j) - hours(starttime(j).Hour)...
                    - minutes(starttime(1).Minute) - seconds(starttime(j).Second);
                get_time = @(x,swidx) datestr(start_of_day + minutes(x(1) + swidx + 1),'HH:MM');
                try obj.Wake_Time_L{j} = get_time(idx_wake_L,wake_idx_1); catch, obj.Wake_Time_L{j} = '00:00'; end %#ok<*FNDSB>
                try obj.Wake_Time_R{j} = get_time(idx_wake_R,wake_idx_1); catch, obj.Wake_Time_R{j} = '00:00'; end
                try obj.Sleep_Time_L{j} = get_time(idx_sleep_L,sleep_idx_1); catch, obj.Sleep_Time_L{j} = '00:00'; end
                try obj.Sleep_Time_R{j} = get_time(idx_sleep_R,sleep_idx_1); catch, obj.Sleep_Time_R{j} = '00:00'; end

                % Save out sleep plots by visit
                for i=1:6
                    F = figure('Position',[1,1,800,400]);
                    bar(sp{i});
                    labeltimes = {'00:00';'02:00';'04:00';'06:00';'08:00';...
                        '10:00';'12:00';'14:00';'16:00';'18:00';...
                        '20:00';'22:00'};
                    title(['Sleep by Visit, Subject: ' obj.Name{j} ' Visit: ' num2str(obj.Visit(j))]);
                    ylim([0, 1]);
                    set(gca,'xtick',1:120:1381,'xticklabel',labeltimes);
                    set(gca,'ytick',0:1,'yticklabel',{'No Sleep','Sleep'});
                    xlabel('Time');
                    ylabel('Sleep');
                    if ~exist([settings.directory filesep 'plots' filesep sp_name{i}], 'dir')
                        mkdir([settings.directory filesep 'plots' filesep sp_name{i}])
                    end
                    saveas(F,[settings.directory filesep 'plots' filesep sp_name{i} filesep obj.Name{j} '_',...
                        num2str(obj.Visit(j)) '_' sp_name{i} '.png']);
                    close(F);
                end
           end
        end
        function sleep_plot = sleepbyage(obj,settings)
            %SLEEPBYAGE - Generate Avg Sleep Plots by Age groups
            disp('Generating Sleep Plot by Age... ');

            % create plots directory if not exist
            if ~exist([settings.directory filesep 'plots'],'dir')
               mkdir([settings.directory filesep 'plots']);
            end

            % create arrays to store sleep plots
            sleep_plot.sl = zeros(max(obj.Age)+2,1440);
            sleep_plot.sr = zeros(max(obj.Age)+2,1440);
            sleep_plot.ckl = zeros(max(obj.Age)+2,1440);
            sleep_plot.ckr = zeros(max(obj.Age)+2,1440);
            sleep_plot.gl = zeros(max(obj.Age)+2,1440);
            sleep_plot.gr = zeros(max(obj.Age)+2,1440);

            % loop through each subject age
            for i=-1:max(obj.Age)
                % find all subjects equal to the current age lookup
                subset_idx = find(obj.Age == i);

                % only process if any subjects in age group
                if sum(subset_idx) ~= 0
                    % convert to datetime objects
                    starttime = datetime(obj.Start(subset_idx));
                    endtime = starttime + minutes(cellfun('length',obj.Sadeh_Sleep_Left(subset_idx))-1);

                    % get subset of sleeps
                    sadeh_left = obj.Sadeh_Sleep_Left(subset_idx);
                    sadeh_right = obj.Sadeh_Sleep_Right(subset_idx);
                    ck_left = obj.CK_Sleep_Left(subset_idx);
                    ck_right = obj.CK_Sleep_Right(subset_idx);
                    g_left = obj.Galland_Sleep_Left(subset_idx);
                    g_right = obj.Galland_Sleep_Right(subset_idx);

                    % loop through each of the visits in the subset
                    for j=1:length(sadeh_left)
                        % Refactor array into a 24 hour span; get the
                        % nearest start and end days
                        beginning_day = starttime(j) - hours(starttime(j).Hour)...
                            - minutes(starttime(1).Minute) - seconds(starttime(j).Second);
                        end_day = endtime(j) - hours(endtime(j).Hour)...
                            - minutes(endtime(j).Minute) - seconds(endtime(j).Second) + caldays(1) - minutes(1);

                        % pad the start and ends of the sleep arrays
                        sadeh_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            sadeh_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                        sadeh_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            sadeh_right{j}; zeros(minutes(end_day-endtime(j)),1)];
                        ck_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            ck_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                        ck_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            ck_right{j}; zeros(minutes(end_day-endtime(j)),1)];
                        g_left{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            g_left{j}; zeros(minutes(end_day-endtime(j)),1)];
                        g_right{j} = [zeros(minutes(starttime(j)-beginning_day),1);...
                            g_right{j}; zeros(minutes(end_day-endtime(j)),1)];

                        % find the number of days in-between the start and
                        % end days
                        time_length = round(days(end_day - beginning_day));

                        % loop through the each day; refactor into 24 hr
                        % period
                        slp = zeros(1440,1);
                        srp = zeros(1440,1);
                        cklp = zeros(1440,1);
                        ckrp = zeros(1440,1);
                        glp = zeros(1440,1);
                        grp = zeros(1440,1);
                        begin = beginning_day;
                        begin_element = 1;
                        for k=1:time_length
                            % find end of day
                            finish = begin + caldays(1) - minutes(1);

                            % get the number of elements between begin and
                            % finish
                            final_element = minutes(finish - begin) + begin_element;

                            % check if the final_element is longer than sadeh_left; limit to last element
                            if final_element > length(sadeh_left{j})
                                final_element = length(sadeh_left{j});
                            end

                            % find difference between final_element and sleep period
                            dif = length(slp) - length(sadeh_left{j}(begin_element:final_element));

                            % refactor days
                            slp = slp + [sadeh_left{j}(begin_element:final_element); zeros(dif,1)];
                            srp = srp + [sadeh_right{j}(begin_element:final_element); zeros(dif,1)];
                            cklp = cklp + [ck_left{j}(begin_element:final_element); zeros(dif,1)];
                            ckrp = ckrp + [ck_right{j}(begin_element:final_element); zeros(dif,1)];
                            glp = glp + [g_left{j}(begin_element:final_element); zeros(dif,1)];
                            grp = grp + [g_right{j}(begin_element:final_element); zeros(dif,1)];

                            % reset for next day
                            begin = begin + caldays(1);
                            begin_element = final_element + 1;
                        end

                        % reassign to initial variable
                        sadeh_left{j} = slp';
                        sadeh_right{j} = srp';
                        ck_left{j} = cklp';
                        ck_right{j} = cklp';
                        g_left{j} = glp';
                        g_right{j} = glp';
                    end

                    % take average of sleep score periods
                    sleep_plot.sl(i+2,:) = mean(cell2mat(sadeh_left),1);
                    sleep_plot.sr(i+2,:) = mean(cell2mat(sadeh_right),1);
                    sleep_plot.ckl(i+2,:) = mean(cell2mat(ck_left),1);
                    sleep_plot.ckr(i+2,:) = mean(cell2mat(ck_right),1);
                    sleep_plot.gl(i+2,:) = mean(cell2mat(g_left),1);
                    sleep_plot.gr(i+2,:) = mean(cell2mat(g_right),1);
                end
            end

            % create cell for sleep plot
            sp{1} = sleep_plot.sl; sp_name{1} = 'sleepbyage_sadeh_left';
            sp{2} = sleep_plot.sr; sp_name{2} = 'sleepbyage_sadeh_right';
            sp{3} = sleep_plot.ckl; sp_name{3} = 'sleepbyage_cole_kripke_left';
            sp{4} = sleep_plot.ckr; sp_name{4} = 'sleepbyage_cole_kripke_right';
            sp{5} = sleep_plot.ckl; sp_name{5} = 'sleepbyage_galland_left';
            sp{6} = sleep_plot.ckr; sp_name{6} = 'sleepbyage_galland_right';

            % set default figure visibility to off
            set(0, 'DefaultFigureVisible', 'off');

            % plot sleep plot and save
            for i=1:6
                F = figure('Position',[1,1,800,400]);
                imagesc(sp{i});
                labeltimes = {'00:00';'02:00';'04:00';'06:00';'08:00';...
                    '10:00';'12:00';'14:00';'16:00';'18:00';...
                    '20:00';'22:00'};
                title('Sleep by Age Group');
                set(gca,'xtick',1:120:1381,'xticklabel',labeltimes);
                set(gca,'ytick',1:length(-1:max(obj.Age)),'yticklabel',{'No Data',0:max(obj.Age)});
                xlabel('Time');
                ylabel('Age');
                colormap(jet);
                colorbar;
                caxis([0, 1]);
                if ~exist([settings.directory filesep 'plots' filesep 'sleepbyage'], 'dir')
                        mkdir([settings.directory filesep 'plots' filesep 'sleepbyage'])
                end
                saveas(F,[settings.directory filesep 'plots' filesep 'sleepbyage' filesep sp_name{i} '.png']);
                close(F);
            end
        end
    end
end
