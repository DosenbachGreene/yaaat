function [sleep, params] = cole_kripke_sleep_detect(y_data, filt_size)
%COLEKRIPKESLEEPDETECT Implementation of the cole-kripke sleep detection algorithm
%   This function expects y-axis accel data with 1 minute epoch blocks
%
% inputs:
%
% y-data: the y-data activity counts vector blocked in 1 minute increments
%
% filt_size specifies the length of the window for median filtering
%
% outputs:
%
% sleep: a logical array containing the times where the epoch is classified
% as sleep/non-sleep (1/0)
%
% params: a structure that contains the parameters to the cole-kripke algorithm;
% this output can usually be ignored with ~
%
% Andrew Van, 01/17/17, vanandrew@wustl.edu

% scale the y-axis data by 100
scale_y_data = 100*y_data;

% check if any activity counts > 300, set to 300 if > 300
scale_y_data(scale_y_data > 300) = 300;

% sum the signal with a weighted 7 minute window
params.sleep_score = 0.001*conv(scale_y_data,[0,0,67,74,230,76,58,54,106],'same');

% check sleep
sleep = params.sleep_score < 1;

% filter with median filter
sleep = logical(medfilt1(double(sleep), filt_size));

% do morphological operation
sleep = imclose(sleep,strel('line',31,0));

end
