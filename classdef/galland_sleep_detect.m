function [sleep, params] = galland_sleep_detect(y_data, filt_size)
%GALLANDSLEEPDETECT Implementation of the count-scaled sleep detection algorithm
%   This function expects y-axis accel data with 1 minute epoch blocks
%
% inputs:
%
% y-data: the y-data activity counts vector blocked in 1 minute increments
%
% filt_size specifies the length of the window for median filtering
%
% outputs:
%
% sleep: a logical array containing the times where the epoch is classified
% as sleep/non-sleep (1/0)
%
% params: a structure that contains the parameters to the cole-kripke algorithm;
% this output can usually be ignored with ~
%
% Andrew Van, 01/17/17, vanandrew@wustl.edu

% sum the signal with a weighted 7 minute window
params.sleep_score = 2.7*conv(0.0033*y_data,[0,0,0.82,4.01,5.05,4.30,2.57,1.09,1.17],'same');

% check sleep
sleep = params.sleep_score < 1;

% filter with median filter
%sleep = logical(medfilt1(double(sleep), filt_size));

% do morphological operation
sleep = imclose(sleep,strel('line',31,0));

end
