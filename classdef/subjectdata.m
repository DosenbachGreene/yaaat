classdef subjectdata < datacontainer
    %SUBJECTDATA - class object for holding subject data
    %
    % Andrew Van, 05/25/17, vanandrew@wustl.edu

    properties
    end

    methods
        function addsubjects(obj,list,settings)
            %ADDSUBJECT - add subjects to data container
            %   input:
            %       list: list of subjects to add to container
            %       settings: settings structure (See YAAAT.m)

            % function for converting time to cell
            format_time = @(x) cellstr(datestr(x));

            % loop through each subject
            for i=1:height(list)
                disp(['Processing Subject Data for ', num2str(list.Name{i})]);

                % load subject data
                load([settings.directory filesep 'data' filesep list.Name{i} '.mat']);

                % preprocess subject data
                sub.makesamelength();
                sub.cutout(settings.cut_size,settings.sample_rate);
                sub.calcresultant();
                sub.detectwear();
                sub.detectsleep();

                % Get the time information for the subject
                one_sample_in_days = (((1/settings.sample_rate)/60)/60)/24;
                time_start = format_time(sub.data.time_left(1));
                time_done = format_time(sub.data.time_left(end) + one_sample_in_days*(length(sub.data.LVMData{end})-1));

                % format data structure
                ds.left = cell2mat(sub.data.LVMData);
                ds.right = cell2mat(sub.data.RVMData);

                % Get the stats for each hand
                stats = calcstats(ds, settings.activity_threshold, settings.sample_rate, sub.hand);

                % Save subject to table3
                obj.addrow(list.Name(i),sub.age,sub.data.V_num(end),{sub.hand},...
                    time_start,time_done,round(stats.Recording_Period*60),...
                    stats.UseRatio,stats.WeightedUseRatio,...
                    stats.UniUseRatio,stats.BiUseRatio,...
                    stats.WeightedUniUseRatio,stats.WeightedBiUseRatio,...
                    stats.LUseCount, stats.RUseCount,...
                    stats.percentnomov,stats.percenttimemov,stats.percentsim,...
                    stats.percentDomUni,stats.percentNDUni,{stats.magnitude},...
                    {stats.ratio},{stats.Uni_Dom_UE},{stats.Uni_ND_UE},...
                    {stats.Dom_UE_bi},{stats.ND_UE_bi},...
                    {cell2mat(sub.data.wearL)},{cell2mat(sub.data.wearR)},...
                    {cell2mat(sub.data.s_sleep_l)},{cell2mat(sub.data.s_sleep_r)},...
                    {cell2mat(sub.data.ck_sleep_l)},{cell2mat(sub.data.ck_sleep_r)},...
                    {cell2mat(sub.data.g_sleep_l)},{cell2mat(sub.data.g_sleep_r)});

                % add subject to data list table
                obj.list.addsubject(list.Name(i),{sub.data.V_num});
            end
        end
        function age = sortbyage(obj,settings)
            %SORTBYAGE - generates age sorted plots and tables

            % initialize table
            disp('Sorting Age Data...');
            age = cell2table(cell(0,37),...
                'VariableNames',{'NumofSubs','Age','Period',...
                    'MeanDomUE','StdDomUE','RangeDomUE','MedianDomUE',...
                    'MeanNDUE','StdNDUE','RangeNDUE','MedianNDUE',...
                    'MeanUseRatio','StdUseRatio','RangeUseRatio','MedianUseRatio',...
                    'MeanBilateralMagnitude','StdBilateralMagnitude',...
                    'RangeBilateralMagnitude','MedianBilateral_Magnitude',...
                    'MeanMagnitudeRatio','StdMagnitudeRatio',...
                    'RangeMagnitudeRatio','MedianMagnitudeRatio',...
                    'LWearPercent', 'RWearPercent',...
                    'Percent_Sadeh_Sleep_Left',...
                    'Total_Sadeh_Sleep_Left',...
                    'Percent_Sadeh_Sleep_Right',...
                    'Total_Sadeh_Sleep_Right',...
                    'Percent_CK_Sleep_Left',...
                    'Total_CK_Sleep_Left',...
                    'Percent_CK_Sleep_Right',...
                    'Total_CK_Sleep_Right',...
                    'Percent_Galland_Sleep_Left',...
                    'Total_Galland_Sleep_Left',...
                    'Percent_Galland_Sleep_Right',...
                    'Total_Galland_Sleep_Right'
                });

            % make zero movement white
            cmap = colormap(jet);
            cmap(1,:) = [1,1,1];

            % set default figure visibility to off
            set(0, 'DefaultFigureVisible', 'off');

            % set age groups
            age_group = [0, 2; 3, 10 ;11, 17];

            % density for group
            group_density = cell(3,1);
            group_mean_mr = zeros(3,1);

            % loop through each age group
            for i=1:3
                % find all subjects equal to the current age lookup
                subset_idx = find(obj.Age >= age_group(i,1) & obj.Age <= age_group(i,2));

                % only process if any subjects in age group
                if sum(subset_idx) ~= 0

                    % get the total time of the age group
                    time = sum(obj.Period(subset_idx));

                    % calculate unilateral times
                    dom_time = (obj.PercentDomUni(subset_idx)/100).*obj.Period(subset_idx);
                    nd_time = (obj.PercentNDUni(subset_idx)/100).*obj.Period(subset_idx);

                    % get stats for unilateral times
                    dom_ue = {mean(dom_time),std(dom_time),range(dom_time),median(dom_time)};
                    nd_ue = {mean(nd_time),std(nd_time),range(nd_time),median(nd_time)};

                    % use ratio
                    use_ratio = {mean(obj.UseRatio(subset_idx)),std(obj.UseRatio(subset_idx)),...
                        range(obj.UseRatio(subset_idx)),median(obj.UseRatio(subset_idx))};

                    % concantenate and get stats for bilateral magnitude
                    bm_concat = cell2mat(obj.BiMagnitude(subset_idx));
                    bm = {mean(bm_concat),std(bm_concat),range(bm_concat),median(bm_concat)};

                    % concatenate and get stats for magnitude ratio
                    mr_concat = cell2mat(obj.BiRatio(subset_idx));
                    mr = {mean(mr_concat),std(mr_concat),range(mr_concat),median(mr_concat)};
                    group_mean_mr(i) = mean(mr_concat);

                    % calculate percentage
                    percentage = @(x) 100*sum(x)./length(x);

                    % concatenate the age table with the current age
                    age = [age; {nnz(subset_idx), [num2str(age_group(i,1)) ' to ' num2str(age_group(i,2))], time,...
                        dom_ue{1}, dom_ue{2}, dom_ue{3}, dom_ue{4},...
                        nd_ue{1}, nd_ue{2}, nd_ue{3}, nd_ue{4},...
                        use_ratio{1}, use_ratio{2}, use_ratio{3}, use_ratio{4},...
                        bm{1}, bm{2}, bm{3}, bm{4},...
                        mr{1}, mr{2}, mr{3}, mr{4},...
                        percentage(cell2mat(obj.LWear(subset_idx))),...
                        percentage(cell2mat(obj.RWear(subset_idx))),...
                        percentage(cell2mat(obj.Sadeh_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.Sadeh_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.Sadeh_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.Sadeh_Sleep_Right(subset_idx))),...
                        percentage(cell2mat(obj.CK_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.CK_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.CK_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.CK_Sleep_Right(subset_idx))),...
                        percentage(cell2mat(obj.Galland_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.Galland_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.Galland_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.Galland_Sleep_Right(subset_idx)))
                    }]; %#ok<AGROW>

                    % calculate 3d histograms
                    [N,C] = MyHist4([[mr_concat;...
                        ones(size(cell2mat(obj.UniDom(subset_idx))))*-7;...
                        ones(size(cell2mat(obj.UniND(subset_idx))))*7],...
                        [bm_concat;...
                        cell2mat(obj.UniDom(subset_idx));...
                        cell2mat(obj.UniND(subset_idx))]]);

                    % plot the density figure
                    group_density{i} = N'/nnz(subset_idx);
                    surfc(C{1,1},C{1,2},N'/nnz(subset_idx),'EdgeColor','none');
					F = gcf;
                    title(['Age Group: ' num2str(age_group(i,1)) ' to ' num2str(age_group(i,2))]);

					% adjust view
                    view(2); xlim([-8 8]); ylim([0 1400]); view(0, 90); set(gcf,'renderer','opengl');

                    % adjust labels
                    xlabel('Magnitude Ratio (ln)'); ylabel('Bilateral Magnitude'); grid off;

                    % adjust axis
                    set(gca, 'xlim', [-8, 8]); set(gca, 'xtick', -8:2:8);
                    set(gca, 'ytick', 0:200:1400); set(gca, 'ylim', [0, 1500]);
                    set(gca, 'LineWidth', 1); set(gca, 'Layer', 'top');
                    set(gcf, 'Color', [1 1 1]); colormap(cmap); caxis([0, 300]);
                    colorbar;

                    % create plots directory if not exist
                    if ~exist([settings.directory filesep 'plots'],'dir')
                       mkdir([settings.directory filesep 'plots']);
                    end

                    % create density plots directory if not exist
                    if ~exist([settings.directory filesep 'plots' filesep 'densityplots'], 'dir')
                        mkdir([settings.directory filesep 'plots' filesep 'densityplots'])
                    end

                    % save plot
                    saveas(F,[settings.directory filesep 'plots' filesep 'densityplots' filesep 'densityplotage_' num2str(i) '.png']);

                    % close figure
                    close(F);
                end
            end

            % plot overlay plots
            n = 1;
            for j=1:3
                for k=j+1:3
                    try
                        % get the idx of where the magnitude ratio line should
                        % be drawn

                        % create overlay plot
                        overlay = -(group_density{j}>5)-2*(group_density{k}>5);
                        surfc(C{1,1},C{1,2},overlay,'EdgeColor','none');
                        % add lines
                        line([group_mean_mr(j),group_mean_mr(j)],[10,1400],'LineWidth',2,'Color',[1 1 0]);
                        line([group_mean_mr(k),group_mean_mr(k)],[10,1400],'LineWidth',2,'Color',[0 1 1]);
                        F = gcf; hold on;
                        title({'Comparing Age Group: ',...
                            [num2str(age_group(j,1)) ' to ' num2str(age_group(j,2))],...
                            [num2str(age_group(k,1)) ' to ' num2str(age_group(k,2))]});

                        % adjust view
                        view(2); xlim([-8 8]); ylim([0 1400]); view(0, 90); set(gcf,'renderer','opengl');

                        % adjust labels
                        xlabel('Magnitude Ratio (ln)'); ylabel('Bilateral Magnitude'); grid off;

                        % adjust axis
                        set(gca, 'xlim', [-6, 6]); set(gca, 'xtick', -6:2:6);
                        set(gca, 'ytick', 0:200:1400); set(gca, 'ylim', [0, 1500]);
                        set(gca, 'LineWidth', 1); set(gca, 'Layer', 'top');
                        short_cmap = colormap(jet); short_cmap(end,:) = [1 1 1];
                        short_cmap(end-1,:) = [1 0 0]; short_cmap(end-2,:) = [0 1 0]; short_cmap(end-3,:) = [0 0 1];
                        set(gcf, 'Color', [1 1 1]); colormap(short_cmap); caxis([-63, 0]);

                        % save plot
                        saveas(F,[settings.directory filesep 'plots' filesep 'densityplots' filesep 'overlayplotage_' num2str(n) '.png']);
                        n = n+1; hold off;
                    catch
                        disp('Could not plot overlay.')
                    end
                end
            end

            % create results directory if not exist
            if ~exist([settings.directory filesep 'results'],'dir')
               mkdir([settings.directory filesep 'results']);
            end

            % write age table to csv
            writetable(age,[settings.directory filesep 'results' filesep 'agesort.csv']);
        end
        function age = sortbyage2(obj,settings)
            %SORTBYAGE - generates age sorted plots and tables

            % initialize table
            disp('Sorting Age Data...');
            age = cell2table(cell(0,37),...
                'VariableNames',{'NumofSubs','Age','Period',...
                    'MeanDomUE','StdDomUE','RangeDomUE','MedianDomUE',...
                    'MeanNDUE','StdNDUE','RangeNDUE','MedianNDUE',...
                    'MeanUseRatio','StdUseRatio','RangeUseRatio','MedianUseRatio',...
                    'MeanBilateralMagnitude','StdBilateralMagnitude',...
                    'RangeBilateralMagnitude','MedianBilateral_Magnitude',...
                    'MeanMagnitudeRatio','StdMagnitudeRatio',...
                    'RangeMagnitudeRatio','MedianMagnitudeRatio',...
                    'LWearPercent', 'RWearPercent',...
                    'Percent_Sadeh_Sleep_Left',...
                    'Total_Sadeh_Sleep_Left',...
                    'Percent_Sadeh_Sleep_Right',...
                    'Total_Sadeh_Sleep_Right',...
                    'Percent_CK_Sleep_Left',...
                    'Total_CK_Sleep_Left',...
                    'Percent_CK_Sleep_Right',...
                    'Total_CK_Sleep_Right',...
                    'Percent_Galland_Sleep_Left',...
                    'Total_Galland_Sleep_Left',...
                    'Percent_Galland_Sleep_Right',...
                    'Total_Galland_Sleep_Right'
                });

            % make zero movement white
            cmap = colormap(jet);
            cmap(1,:) = [1,1,1];

            % set default figure visibility to off
            set(0, 'DefaultFigureVisible', 'off');

            % set age groups
            age_group = [0,0;1,1;2,2;3,3;4,4;5,5;6,6;7,7;8,8;9,9;...
                10,10;11,11;12,12;13,13;14,14;15,15;16,16;17,17];

            % density for group
            group_density = cell(18,1);
            group_mean_mr = zeros(18,1);

            % loop through each age group
            for i=1:18
                % find all subjects equal to the current age lookup
                subset_idx = find(obj.Age >= age_group(i,1) & obj.Age <= age_group(i,2));

                % only process if any subjects in age group
                if sum(subset_idx) ~= 0

                    % get the total time of the age group
                    time = sum(obj.Period(subset_idx));

                    % calculate unilateral times
                    dom_time = (obj.PercentDomUni(subset_idx)/100).*obj.Period(subset_idx);
                    nd_time = (obj.PercentNDUni(subset_idx)/100).*obj.Period(subset_idx);

                    % get stats for unilateral times
                    dom_ue = {mean(dom_time),std(dom_time),range(dom_time),median(dom_time)};
                    nd_ue = {mean(nd_time),std(nd_time),range(nd_time),median(nd_time)};

                    % use ratio
                    use_ratio = {mean(obj.UseRatio(subset_idx)),std(obj.UseRatio(subset_idx)),...
                        range(obj.UseRatio(subset_idx)),median(obj.UseRatio(subset_idx))};

                    % concantenate and get stats for bilateral magnitude
                    bm_concat = cell2mat(obj.BiMagnitude(subset_idx));
                    bm = {mean(bm_concat),std(bm_concat),range(bm_concat),median(bm_concat)};

                    % concatenate and get stats for magnitude ratio
                    mr_concat = cell2mat(obj.BiRatio(subset_idx));
                    mr = {mean(mr_concat),std(mr_concat),range(mr_concat),median(mr_concat)};
                    group_mean_mr(i) = mean(mr_concat);

                    % calculate percentage
                    percentage = @(x) 100*sum(x)./length(x);

                    % concatenate the age table with the current age
                    age = [age; {nnz(subset_idx), [num2str(age_group(i,1))], time,...
                        dom_ue{1}, dom_ue{2}, dom_ue{3}, dom_ue{4},...
                        nd_ue{1}, nd_ue{2}, nd_ue{3}, nd_ue{4},...
                        use_ratio{1}, use_ratio{2}, use_ratio{3}, use_ratio{4},...
                        bm{1}, bm{2}, bm{3}, bm{4},...
                        mr{1}, mr{2}, mr{3}, mr{4},...
                        percentage(cell2mat(obj.LWear(subset_idx))),...
                        percentage(cell2mat(obj.RWear(subset_idx))),...
                        percentage(cell2mat(obj.Sadeh_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.Sadeh_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.Sadeh_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.Sadeh_Sleep_Right(subset_idx))),...
                        percentage(cell2mat(obj.CK_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.CK_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.CK_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.CK_Sleep_Right(subset_idx))),...
                        percentage(cell2mat(obj.Galland_Sleep_Left(subset_idx))),...
                        nnz(cell2mat(obj.Galland_Sleep_Left(subset_idx))),...
                        percentage(cell2mat(obj.Galland_Sleep_Right(subset_idx))),...
                        nnz(cell2mat(obj.Galland_Sleep_Right(subset_idx)))
                    }]; %#ok<AGROW>

                    % calculate 3d histograms
                    [N,C] = MyHist4([[mr_concat;...
                        ones(size(cell2mat(obj.UniDom(subset_idx))))*-7;...
                        ones(size(cell2mat(obj.UniND(subset_idx))))*7],...
                        [bm_concat;...
                        cell2mat(obj.UniDom(subset_idx));...
                        cell2mat(obj.UniND(subset_idx))]]);

                    % plot the density figure
                    group_density{i} = N'/nnz(subset_idx);
                    surfc(C{1,1},C{1,2},N'/nnz(subset_idx),'EdgeColor','none');
					F = gcf;
                    title(['Age Group: ' num2str(age_group(i,1))]);

					% adjust view
                    view(2); xlim([-8 8]); ylim([0 1400]); view(0, 90); set(gcf,'renderer','opengl');

                    % adjust labels
                    xlabel('Magnitude Ratio (ln)'); ylabel('Bilateral Magnitude'); grid off;

                    % adjust axis
                    set(gca, 'xlim', [-8, 8]); set(gca, 'xtick', -8:2:8);
                    set(gca, 'ytick', 0:200:1400); set(gca, 'ylim', [0, 1500]);
                    set(gca, 'LineWidth', 1); set(gca, 'Layer', 'top');
                    set(gcf, 'Color', [1 1 1]); colormap(cmap); caxis([0, 300]);
                    colorbar;

                    % create plots directory if not exist
                    if ~exist([settings.directory filesep 'plots'],'dir')
                       mkdir([settings.directory filesep 'plots']);
                    end

                    % create density plots directory if not exist
                    if ~exist([settings.directory filesep 'plots' filesep 'densityplots'], 'dir')
                        mkdir([settings.directory filesep 'plots' filesep 'densityplots'])
                    end

                    % save plot
                    saveas(F,[settings.directory filesep 'plots' filesep 'densityplots' filesep 'densityplotage_' num2str(i) '_2.png']);

                    % close figure
                    close(F);
                end
            end

            % plot overlay plots
            n = 1;
            for j=1:18
                for k=j+1:18
                    try
                    % get the idx of where the magnitude ratio line should
                    % be drawn

                    % create overlay plot
                    overlay = -(group_density{j}>5)-2*(group_density{k}>5);
                    surfc(C{1,1},C{1,2},overlay,'EdgeColor','none');
                    % add lines
                    line([group_mean_mr(j),group_mean_mr(j)],[10,1400],'LineWidth',2,'Color',[1 1 0]);
                    line([group_mean_mr(k),group_mean_mr(k)],[10,1400],'LineWidth',2,'Color',[0 1 1]);
                    F = gcf; hold on;
                    title({'Comparing Age Group: ',...
                        [num2str(age_group(j,1)) ' to ' num2str(age_group(j,2))],...
                        [num2str(age_group(k,1)) ' to ' num2str(age_group(k,2))]});

                    % adjust view
                    view(2); xlim([-8 8]); ylim([0 1400]); view(0, 90); set(gcf,'renderer','opengl');

                    % adjust labels
                    xlabel('Magnitude Ratio (ln)'); ylabel('Bilateral Magnitude'); grid off;

                    % adjust axis
                    set(gca, 'xlim', [-6, 6]); set(gca, 'xtick', -6:2:6);
                    set(gca, 'ytick', 0:200:1400); set(gca, 'ylim', [0, 1500]);
                    set(gca, 'LineWidth', 1); set(gca, 'Layer', 'top');
                    short_cmap = colormap(jet); short_cmap(end,:) = [1 1 1];
                    short_cmap(end-1,:) = [1 0 0]; short_cmap(end-2,:) = [0 1 0]; short_cmap(end-3,:) = [0 0 1];
                    set(gcf, 'Color', [1 1 1]); colormap(short_cmap); caxis([-63, 0]);

                    % save plot
                    saveas(F,[settings.directory filesep 'plots' filesep 'densityplots' filesep 'overlayplotage_' num2str(n) '_2.png']);
                    n = n+1; hold off;
                    catch
                    end
                end
            end

            % create results directory if not exist
            if ~exist([settings.directory filesep 'results'],'dir')
               mkdir([settings.directory filesep 'results']);
            end

            % write age table to csv
            writetable(age,[settings.directory filesep 'results' filesep 'agesort_2.csv']);
        end
        function plotavgdensitysubject(obj,name,settings)
            %PLOTAVGDENSITYSUBJECT - plots average density plots by subject

            disp('Generating Density Plots...');

            % make zero movement white
            cmap = colormap(jet);
            cmap(1,:) = [1,1,1];

            % set default figure visibility to off
            set(0, 'DefaultFigureVisible', 'off');

            % get list of subjects to generate plots
            if ~settings.force_process_all
                sublist = obj.Name(ismember(obj.Name,name));
            else
                sublist = obj.Name;
            end

            % loop over subjects
            for i=1:length(sublist)
                disp(['Generating Plot for ' sublist{i}])

                % calculate 3d histograms
                [N,C] = MyHist4([[obj.BiRatio{i};...
                    ones(size(obj.UniDom{i}))*-7;...
                    ones(size(obj.UniND{i}))*7],...
                    [obj.BiMagnitude{i};...
                    obj.UniDom{i};obj.UniND{i}]]);

                % plot the density figure
                surfc(C{1,1},C{1,2},N','EdgeColor','none');
				F = gcf;
                title(['Subject: ' sublist{i}]);

                % adjust view
                view(2);
                xlim([-8 8]);
                ylim([0 1400]);
                view(0, 90);
                set(gcf,'renderer','opengl');

                % adjust labels
                xlabel('Magnitude Ratio (ln)');
                ylabel('Bilateral Magnitude');
                grid off;

                % adjust axis
                set(gca, 'ylim', [0, 900]);
                set(gca, 'xtick', -8:2:8);
                set(gca, 'ytick', 0:200:1400);
                set(gca, 'xlim', [-8, 8]);
                set(gca, 'ylim', [0, 1500]);
                set(gca, 'LineWidth', 1);
                set(gca, 'Layer', 'top');
                set(gcf, 'Color', [1 1 1]);
                colormap(cmap);
                caxis([0, 300]);
                colorbar;

                % create plots directory if not exist
                if ~exist([settings.directory filesep 'plots'],'dir')
                   mkdir([settings.directory filesep 'plots']);
                end

                % create density plots directory if not exist
                if ~exist([settings.directory filesep 'plots' filesep 'densityplots'], 'dir')
                    mkdir([settings.directory filesep 'plots' filesep 'densityplots'])
                end

                % save plot
                saveas(F,[settings.directory filesep 'plots' filesep 'densityplots' filesep obj.Name{i} '.png']);

                % close figure
                close(F);

                % Create Unilateral Histogram Plots
                U = figure;
                histogram((obj.UniDom{i}*-1),100,'facecolor','g','facealpha',.5,'edgecolor','none'); hold on;
                histogram(obj.UniND{i},100,'facecolor','r','facealpha',.5,'edgecolor','none');
                title(['Subject: ' sublist{i}]);
                xlabel('                 Dominant Hand Magnitudes                                  Non-Dominant Hand Magnitudes ')
                saveas(U,[settings.directory filesep 'plots' filesep 'densityplots' filesep obj.Name{i} '_Unilateral_DOMtoND_histogram.png']);
                close(U);

                % Create Bilateral Histogram Plots
                B = figure;
                histogram((obj.BiDom{i}*-1),100,'facecolor','g','facealpha',.5,'edgecolor','none'); hold on;
                histogram(obj.BiND{i},100,'facecolor','r','facealpha',.5,'edgecolor','none');
                title(['Subject: ' sublist{i}]);
                xlabel('                 Dominant Hand Magnitudes                                  Non-Dominant Hand Magnitudes ')
                saveas(B,[settings.directory filesep 'plots' filesep 'densityplots' filesep obj.Name{i} '_Bilateral_DOMtoND_histogram.png']);
                close(B);
            end
        end
    end
end
