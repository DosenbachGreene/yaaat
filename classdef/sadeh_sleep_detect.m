function [sleep, params] = sadeh_sleep_detect(y_data, filt_size)
%SADEHSLEEPDETECT Implementation of the sadeh sleep detection algorithm
%   This function expects y-axis accel data with 1 minute epoch blocks
%
% inputs:
%
% y-data: the y-data activity counts vector blocked in 1 minute increments
%
% filt_size specifies the length of the window for median filtering
%
% outputs:
%
% sleep: a logical array containing the times where the epoch is classified
% as sleep/non-sleep (1/0)
%
% params: a structure that contains the parameters to the sadeh algorithm;
% this output can usually be ignored with ~
%
% Andrew Van, 01/17/17, vanandrew@wustl.edu

% check if any activity counts > 300, set to 300 if > 300
y_data(y_data > 300) = 300;

% get the mean of the activity counts across the window
params.AVG = conv(y_data, (1/11)*ones(1,11), 'same');

% find if each epoch >= 50 & epoch < 100
NATS_epoch = double((y_data >= 50) & (y_data < 100));

% get the number of epochs that have counts >= 50 and < 100
params.NATS = conv(NATS_epoch, ones(1,11), 'same');

% get the std for the first epochs of the window
n = conv(ones(1,length(y_data)), [zeros(1,5), ones(1,6)], 'same'); % number of elements in 1st half of window
s = conv(y_data, [zeros(1,5), ones(1,6)], 'same'); % summation of element in window
q = conv(y_data.^2, [zeros(1,5), ones(1,6)], 'same'); % sum of squares
raw_SD = ((q - (s.^2)./n)./(n-1)).^0.5; % calculate std
params.SD = zeros(1,length(raw_SD)); % create empty zero vector
params.SD(~isnan(raw_SD)) = raw_SD(~isnan(raw_SD)); % remove NaN

% get the natural logarithm of each epoch
raw_LG = log(y_data);
params.LG = zeros(1,length(raw_LG)); % create empty zero vector
params.LG(y_data~=0) = raw_LG(y_data~=0); % remove NaN

% calculate the sleep score
params.sleep_score = (7.601-(0.065*params.AVG)-(1.08*params.NATS)-(0.056*params.SD)-(0.703*params.LG));

% detect sleep if > -4
sleep = (params.sleep_score > -4);

% medfilt the sleep signal
%sleep = logical(medfilt1(double(sleep), filt_size));

% do morphological operation
sleep = imclose(sleep,strel('line',31,0));


end