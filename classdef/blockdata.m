classdef blockdata < datacontainer
    %BLOCKDATA - class object for holding blocked data
    %
    % Andrew Van, 05/24/17, vanandrew@wustl.edu

    properties
    end

    methods
        function addrow(obj,varargin)
            %ADDROW - adds row to datacontainer
            %   Inputs:
            %       Name,Age,Visit,Dominant Hand, Start Time, End Time, Period Record,
            %       Use Ration, Left Use Count, Right Use Count, Percent No Movement,
            %       Percent Movement, Percent Simultaneous Movement, Percent Dominant Unilateral,
            %       Percent Non-Dominant Unilateral, Bilateral Magnitude, Bilateral Ratio,
            %       Unilateral Dominant Magnitude, Unilateral Non-Dominant Magnitude

            % Check if number of inputs OK
            assert(nargin == 25, 'Invalid number of Arguments');

            % add data row to list
            obj.Name = [obj.Name; varargin{1}];
            obj.Age = [obj.Age; varargin{2}];
            obj.Visit = [obj.Visit; varargin{3}];
            obj.Domhand = [obj.Domhand; varargin{4}];
            obj.Start = [obj.Start; varargin{5}];
            obj.End = [obj.End; varargin{6}];
            obj.Period = [obj.Period; varargin{7}];
            obj.UseRatio = [obj.UseRatio; varargin{8}];
            obj.wUseRatio = [obj.wUseRatio; varargin{9}];
            obj.UniUseRatio = [obj.UniUseRatio; varargin{10}];
            obj.BiUseRatio = [obj.BiUseRatio; varargin{11}];
            obj.wUniUseRatio = [obj.wUniUseRatio; varargin{12}];
            obj.wBiUseRatio = [obj.wBiUseRatio; varargin{13}];
            obj.LUseCount = [obj.LUseCount; varargin{14}];
            obj.RUseCount = [obj.RUseCount; varargin{15}];
            obj.PercentNoMov = [obj.PercentNoMov; varargin{16}];
            obj.PercentMov = [obj.PercentMov; varargin{17}];
            obj.PercentSim = [obj.PercentSim; varargin{18}];
            obj.PercentDomUni = [obj.PercentDomUni; varargin{19}];
            obj.PercentNDUni = [obj.PercentNDUni; varargin{20}];
            obj.BiMagnitude = [obj.BiMagnitude; varargin{21}];
            obj.BiRatio = [obj.BiRatio; varargin{22}];
            obj.UniDom = [obj.UniDom; varargin{23}];
            obj.UniND = [obj.UniND; varargin{24}];
        end
        function clearsubject(obj,name)
            %CLEARSUBJECT - Delete subject from datalist

            % Find index of subject name
            idx = ismember(obj.Name,name);

            % clear data of subject
            obj.Name(idx) = [];
            obj.Age(idx) = [];
            obj.Visit(idx) = [];
            obj.Domhand(idx) = [];
            obj.Start(idx) = [];
            obj.End(idx) = [];
            obj.Period(idx) = [];
            obj.UseRatio(idx) = [];
            obj.UniUseRatio(idx) = [];
            obj.BiUseRatio(idx) = [];
            obj.wUseRatio(idx) = [];
            obj.wUniUseRatio(idx) = [];
            obj.wBiUseRatio(idx) = [];
            obj.LUseCount(idx) = [];
            obj.RUseCount(idx) = [];
            obj.PercentNoMov(idx) = [];
            obj.PercentMov(idx) = [];
            obj.PercentSim(idx) = [];
            obj.PercentDomUni(idx) = [];
            obj.PercentNDUni(idx) = [];
            obj.BiMagnitude(idx) = [];
            obj.BiRatio(idx) = [];
            obj.UniDom(idx) = [];
            obj.UniND(idx) = [];
        end
        function writecsvfile(obj,settings,name)
            %WRITECSVFILE - Writes datalist to human readable CSV format

            % construct table for writing
            tw = [table(obj.Name,obj.Age,obj.Visit,obj.Domhand,obj.Start,obj.End,...
                obj.Period,obj.UseRatio,obj.UniUseRatio,obj.BiUseRatio,obj.wUseRatio,obj.wUniUseRatio,obj.wBiUseRatio,...
                obj.LUseCount,obj.RUseCount,obj.PercentNoMov,...
                obj.PercentMov,obj.PercentSim,obj.PercentDomUni,obj.PercentNDUni,'VariableNames',...
                {'Name','Age','Visit','Domhand','Start','End','Period',...
                'UseRatio','UniUseRatio','BiUseRatio','wUseRatio','wUniUseRatio','wBiUseRatio',...
                'LUseCount','RUseCount','PercentNoMov','PercentMov','PercentSim','PercentDomUni','PercentNDUni'}),...
                table(cellfun(@median,obj.BiMagnitude),...
                    cellfun(@max,obj.BiMagnitude),...
                    cellfun(@min,obj.BiMagnitude),...
                    cellfun(@median,obj.BiRatio),...
                    cellfun(@max,obj.BiRatio),...
                    cellfun(@min,obj.BiRatio),...
                    'VariableNames',{...
                    'MedianBilateralMagnitude','MaxBilateralMagnitude',...
                    'MinBilateralMagnitude','MedianMagnitudeRatio',...
                    'MaxMagnitudeRatio','MinMagnitudeRatio'})];

            % sort table
            tw = sortrows(tw,'Visit','ascend');
            tw = sortrows(tw,'Name','ascend');

             % create results directory if not exist
             if ~exist([settings.directory filesep 'results'],'dir')
                mkdir([settings.directory filesep 'results']);
             end

             % write table to csv
             writetable(tw,[settings.directory filesep 'results' filesep name '.csv']);
        end
        function addsubjects(obj,list,settings)
            %ADDSUBJECT - add subjects to data container
            %   input:
            %       list: list of subjects to add to container
            %       settings: settings structure (See YAAAT.m)

            % function for converting time to cell
            format_time = @(x) cellstr(datestr(x));

            % loop through each subject
            for i=1:height(list)
                disp(['Processing Blocked Data for ', num2str(list.Name{i})]);

                % load subject data
                load([settings.directory filesep 'data' filesep list.Name{i} '.mat']);

                % preprocess subject data
                sub.makesamelength();
                sub.cutout(settings.cut_size,settings.sample_rate);
                sub.calcresultant();

                % find the number of points in one block
                points_per_block = round(settings.sample_rate*settings.block_size*60);

                % process each visit
                for j=1:height(sub.data)
                    % Calcluate the number of blocks based on the user specified
                    % block settings
                    num_blocks = ceil(length(sub.data.LVMData{j})/points_per_block);

                    b_start = 1; % Index for start of block
                    for block_idx = 1:num_blocks
                        % Get the end index for the current block
                        b_end = points_per_block*block_idx;

                        % Check if the end index exceeds the array size; if it does
                        % assign the last value of the array to the end index
                        if length(sub.data.LVMData{j}) < b_end
                           b_end = length(sub.data.LVMData{j});
                        end

                        % Get the time information for the block
                        one_sample_in_days = (((1/settings.sample_rate)/60)/60)/24;
                        time_start = format_time(sub.data.time_left(j) + one_sample_in_days*(b_start-1));
                        time_done = format_time(sub.data.time_left(j) + one_sample_in_days*(b_end-1));

                        % format data structure
                        ds.left = sub.data.LVMData{j}(b_start:b_end);
                        ds.right = sub.data.RVMData{j}(b_start:b_end);

                        % Get the stats for each hand
                        stats = calcstats(ds, settings.activity_threshold, settings.sample_rate, sub.hand);

                        % Save block to table
                        obj.addrow(list.Name(i),sub.age,sub.data.V_num(j),{sub.hand},...
                            time_start,time_done,round(stats.Recording_Period*60),...
                            stats.UseRatio,stats.WeightedUseRatio,...
                            stats.UniUseRatio,stats.BiUseRatio,...
                            stats.WeightedUniUseRatio,stats.WeightedBiUseRatio,...
                            stats.LUseCount, stats.RUseCount,...
                            stats.percentnomov,stats.percenttimemov,stats.percentsim,...
                            stats.percentDomUni,stats.percentNDUni,{stats.magnitude},...
                            {stats.ratio},{stats.Uni_Dom_UE},{stats.Uni_ND_UE});

                        % Setup for the next block; set the end of the current block + 1
                        % to the start of the next block
                        b_start = b_end+1;
                    end
                end

                % add subject to data list table
                obj.list.addsubject(list.Name(i),{sub.data.V_num});
            end
        end
    end
end
