classdef subject < handle
    %SUBJECT - properties and methods for subject class
    %   properties:
    %       name: subject id
    %       age: age of subject
    %       hand: handedness; can be 'RH' or 'LH'
    %       data: table of accelerometry data
    %   methods:
    %       contructor: accepts initilization vales for properties
    %       makesamelength: makes the data L/R the same length
    %       cutout: cuts out specified chunk size from start and end
    %       calcresultant: calculate vector sum for data in L/R
    %
    %   Andrew Van, 06/23/17, vanandrew@wustl.edu

    properties
        name;
        age;
        hand;
        data;
    end

    methods
        function obj = subject(varargin)
            %SUBJECT - Constructor for subject object

            % assert number of arguments
            assert(nargin <= 4,'Too many arguments');
            if nargin >= 1
                obj.name = varargin{1};
            end
            if nargin >= 2
                obj.age = varargin{2};
            end
            if nargin >= 3
                obj.hand = varargin{3};
            end
            if nargin >= 4
                obj.data = varargin{4};
            end
        end
        function makesamelength(obj)
            %MAKESAMELENGTH - makes the data L/R the same length

            % assert that data is not empty
            assert(~isempty(obj.data),'data is not set for subject');

            % add extra points column to table
            obj.data = [obj.data, table(zeros(height(obj.data),1),'VariableNames',{'extra_points'})];

            % loop over each visit
            for i = 1:height(obj.data)
                % Check if left and right are same size; resize if not
                if length(obj.data.raw_data_left{i}(:,1)) > length(obj.data.raw_data_right{i}(:,1))
                    obj.data.extra_points(i) = length(obj.data.raw_data_left{i}(:,1)) - length(obj.data.raw_data_right{i}(:,1));
                    obj.data.raw_data_left{i} = obj.data.raw_data_left{i}(1:end-obj.data.extra_points(i),:);
                elseif length(obj.data.raw_data_right{i}(:,1)) > length(obj.data.raw_data_left{i}(:,1))
                    obj.data.extra_points(i) = length(obj.data.raw_data_right{i}(:,1)) - length(obj.data.raw_data_left{i}(:,1));
                    obj.data.raw_data_right{i} = obj.data.raw_data_right{i}(1:end-obj.data.extra_points(i),:);
                else
                    obj.data.extra_points(i) = 0;
                end
            end
        end
        function cutout(obj,chunk_size,sample_rate)
            %CUTOUT - cuts out specified chunk size from start and end

            % assert that data is not empty
            assert(~isempty(obj.data),'data is not set for subject');

            % Calculate the number of points to cut
            cut_points = ceil(chunk_size*sample_rate*60);

            % loop over each visit
            for i = 1:height(obj.data)
                % Cut the points out
                obj.data.raw_data_left{i} = obj.data.raw_data_left{i}(cut_points+1:end-cut_points,:);
                obj.data.raw_data_right{i} = obj.data.raw_data_right{i}(cut_points+1:end-cut_points,:);
            end
        end
        function calcresultant(obj)
            %CALCRESULTANT - calculate vector sum for data in L/R

            % assert that data is not empty
            assert(~isempty(obj.data),'data is not set for subject');

            % add resultant columns
            obj.data = [obj.data, table(cell(height(obj.data),1),cell(height(obj.data),1),...
                'VariableNames',{'LVMData','RVMData'})];

            % loop over each visit
            for i = 1:height(obj.data)
                % Take out data we want and process
                XData = obj.data.raw_data_left{i}(:,1);
                YData = obj.data.raw_data_left{i}(:,2);
                ZData = obj.data.raw_data_left{i}(:,3);

                % Find no data sections
                idx = (XData == -1);

                % Convert to Vector magnitude
                obj.data.LVMData{i} = sqrt((XData.^2) + (YData.^2) + (ZData.^2));

                % Replace no data section using the no data indicies
                obj.data.LVMData{i}(idx) = -1;

                % Take out data we want and process
                XData = obj.data.raw_data_right{i}(:,1);
                YData = obj.data.raw_data_right{i}(:,2);
                ZData = obj.data.raw_data_right{i}(:,3);

                % Find no data sections
                idx = (XData == -1);

                % Convert to Vector magnitude
                obj.data.RVMData{i} = sqrt((XData.^2) + (YData.^2) + (ZData.^2));

                % Replace no data section using the no data indicies
                obj.data.RVMData{i}(idx) = -1;
            end
        end
        function changeepoch(obj,direction,epoch_size)
            %CHANGEEPOCH - change the block/epoch size of a signal

            % Only run if calcresultant has been run
            assert(logical(sum((strcmp('LVMData',obj.data.Properties.VariableNames) |...
                strcmp('RVMData',obj.data.Properties.VariableNames)))),...
                'LVMData/RVMData does not exist.');

            % Initialize new arrays
            if direction == 'x'
                obj.data.NewXLeft = cell(height(obj.data),1);
                obj.data.NewXLeftPS = cell(height(obj.data),1);
                obj.data.NewXRight = cell(height(obj.data),1);
                obj.data.NewXRightPS = cell(height(obj.data),1);
            elseif direction == 'y'
                obj.data.NewYLeft = cell(height(obj.data),1);
                obj.data.NewYLeftPS = cell(height(obj.data),1);
                obj.data.NewYRight = cell(height(obj.data),1);
                obj.data.NewYRightPS = cell(height(obj.data),1);
            elseif direction == 'z'
                obj.data.NewZLeft = cell(height(obj.data),1);
                obj.data.NewZLeftPS = cell(height(obj.data),1);
                obj.data.NewZRight = cell(height(obj.data),1);
                obj.data.NewZRightPS = cell(height(obj.data),1);
            elseif direction == 'all'
                obj.data.LVMDataNew = cell(height(obj.data),1);
                obj.data.LVMDataNewPS = cell(height(obj.data),1);
                obj.data.RVMDataNew = cell(height(obj.data),1);
                obj.data.RVMDataNewPS = cell(height(obj.data),1);
            else
                error('Invalid Direction');
            end

            for i=1:height(obj.data)
                % assign data to signal
                if direction == 'x'
                	signalL = obj.data.raw_data_left{i}(:,1);
                    signalR = obj.data.raw_data_right{i}(:,1);
                elseif direction == 'y'
                    signalL = obj.data.raw_data_left{i}(:,2);
                    signalR = obj.data.raw_data_right{i}(:,2);
                elseif direction == 'z'
                    signalL = obj.data.raw_data_left{i}(:,2);
                    signalR = obj.data.raw_data_right{i}(:,2);
                elseif direction == 'all'
                    signalL = obj.data.LVMData{i};
                    signalR = obj.data.RVMData{i};
                else
                    error('Invalid Direction');
                end

                % get the size of the signal
                [r,c] = size(signalL);

                % make sure the signal is in the proper dimensions
                if r < c
                    signalL = signalL';
                end
                assert(size(signalL,1) == numel(signalL));

                % create a window for summing of size "epoch_size" (sum forward)
                window = [ones(epoch_size,1); zeros(epoch_size-1,1)];

                % sum using convolution
                conv_signal = conv(signalL,window,'same');

                % obtain the size of each epoch by convoluting a ones vector of the same
                % length
                conv_points = conv(ones(numel(signalL),1),window,'same');

                % get the new signal/point size vectors through indexing
                if direction == 'x'
                	obj.data.NewXLeft{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewXLeftPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'y'
                    obj.data.NewYLeft{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewYLeftPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'z'
                    obj.data.NewZLeft{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewZLeftPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'all'
                    obj.data.LVMDataNew{i} = conv_signal(1:epoch_size:end);
                    obj.data.LVMDataNewPS{i} = conv_points(1:epoch_size:end);
                else
                    error('Invalid Direction');
                end

                % get the size of the signal
                [r,c] = size(signalR);

                % make sure the signal is in the proper dimensions
                if r < c
                    signalR = signalR';
                end
                assert(size(signalR,1) == numel(signalR));

                % create a window for summing of size "epoch_size" (sum forward)
                window = [ones(epoch_size,1); zeros(epoch_size-1,1)];

                % sum using convolution
                conv_signal = conv(signalR,window,'same');

                % obtain the size of each epoch by convoluting a ones vector of the same
                % length
                conv_points = conv(ones(numel(signalR),1),window,'same');

                % get the new signal/point size vectors through indexing
                if direction == 'x'
                	obj.data.NewXRight{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewXRightPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'y'
                    obj.data.NewYRight{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewYRightPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'z'
                    obj.data.NewZRight{i} = conv_signal(1:epoch_size:end);
                    obj.data.NewZRightPS{i} = conv_points(1:epoch_size:end);
                elseif direction == 'all'
                    obj.data.RVMDataNew{i} = conv_signal(1:epoch_size:end);
                    obj.data.RVMDataNewPS{i} = conv_points(1:epoch_size:end);
                else
                    error('Invalid Direction');
                end
            end
        end
        function detectwear(obj)
            %DETECTWEAR - detect if subject wore the devices

            % calculate new epochs (minutes)
            obj.changeepoch('all',60);

            % setup 90-min time window
            window1 = ones(90,1);

			% setup artifact window
			artwindow = ones(2,1);

            % setup 30-min window upstream/downstream of 2-min artifact window
            window2 = [ones(30,1); zeros(2,1); ones(30,1)];

            % initialize table for wear/non-wear
            obj.data.wearL = cell(height(obj.data),1);
            obj.data.wearR = cell(height(obj.data),1);

			% define wear detect algorithm
			weartime = @(x) logical(conv(...
				conv(double(logical(x)),window2,'same').*...
				conv(double(logical(x)),artwindow,'same'),...
				window1,'same'));

            % run algorithm on each signal (each visit)
            for i=1:height(obj.data)
                % assign if wear detected
                obj.data.wearL{i} = weartime(obj.data.LVMDataNew{i});
                obj.data.wearR{i} = weartime(obj.data.RVMDataNew{i});
            end
        end
        function detectsleep(obj)
            %DETECTSLEEP - detect sleep throughout each subject visit

            % calculate new epochs (minutes)
            obj.changeepoch('y',60);

            % Initialize new arrays
            obj.data.ck_sleep_l = cell(height(obj.data),1);
            obj.data.ck_sleep_r = cell(height(obj.data),1);
            obj.data.s_sleep_l = cell(height(obj.data),1);
            obj.data.s_sleep_r = cell(height(obj.data),1);
            obj.data.g_sleep_l = cell(height(obj.data),1);
            obj.data.g_sleep_r = cell(height(obj.data),1);

            % call each sleep detection algorithm on data
            for i=1:height(obj.data)
                obj.data.ck_sleep_l{i} = cole_kripke_sleep_detect(obj.data.NewYLeft{i},1);
                obj.data.ck_sleep_r{i} = cole_kripke_sleep_detect(obj.data.NewYRight{i},1);
                obj.data.s_sleep_l{i} = sadeh_sleep_detect(obj.data.NewYLeft{i}',1)';
                obj.data.s_sleep_r{i} = sadeh_sleep_detect(obj.data.NewYRight{i}',1)';
                obj.data.g_sleep_l{i} = galland_sleep_detect(obj.data.NewYLeft{i}',1)';
                obj.data.g_sleep_r{i} = galland_sleep_detect(obj.data.NewYRight{i}',1)';
            end
        end
    end
end
