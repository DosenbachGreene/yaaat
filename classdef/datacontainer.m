classdef datacontainer < handle
    %DATACONTAINER - parent class defining common properties and methods
    %for sub data classes (see blockdata/subjectdata/visitdata class defs)
    %
    % Andrew Van, 05/24/17, vanandrew@wustl.edu

    properties
        Name = {};
        Age = [];
        Visit = [];
        Domhand = {};
        Start = {};
        End = {};
        Period = [];
        UseRatio = [];
        UniUseRatio = [];
        BiUseRatio = [];
        wUseRatio = [];
        wUniUseRatio = [];
        wBiUseRatio = [];
        LUseCount = [];
        RUseCount = [];
        PercentNoMov = [];
        PercentMov = [];
        PercentSim = [];
        PercentDomUni = []
        PercentNDUni = [];
        BiMagnitude = {};
        BiRatio = {};
        UniDom = {};
        UniND = {};
        BiDom = {};
        BiND = {};
        LWear = {};
        RWear = {};
        Sadeh_Sleep_Left = {};
        Sadeh_Sleep_Right = {};
        CK_Sleep_Left = {};
        CK_Sleep_Right = {};
        Galland_Sleep_Left = {};
        Galland_Sleep_Right = {};
        Wake_Time_R = {};
        Wake_Time_L = {};
        Sleep_Time_R = {};
        Sleep_Time_L = {};
        list = manifest;
    end

    methods
        function addrow(obj,varargin)
            %ADDROW - adds row to datacontainer
            %   Inputs:
            %       Name,Age,Visit,Dominant Hand, Start Time, End Time, Period Record,
            %       Use Ration, Left Use Count, Right Use Count, Percent No Movement,
            %       Percent Movement, Percent Simultaneous Movement, Percent Dominant Unilateral,
            %       Percent Non-Dominant Unilateral, Bilateral Magnitude, Bilateral Ratio,
            %       Unilateral Dominant Magnitude, Unilateral Non-Dominant Magnitude,
            %       Left Wear Time, Right Wear Time, Sadeh Sleep Left, Sadeh Sleep Right,
            %       Cole-Kripke Sleep Left, Cole-Kripke Sleep Right

            % Check if number of inputs OK
            assert(nargin == 35, 'Invalid number of Arguments');

            % add data row to list
            obj.Name = [obj.Name; varargin{1}];
            obj.Age = [obj.Age; varargin{2}];
            obj.Visit = [obj.Visit; varargin{3}];
            obj.Domhand = [obj.Domhand; varargin{4}];
            obj.Start = [obj.Start; varargin{5}];
            obj.End = [obj.End; varargin{6}];
            obj.Period = [obj.Period; varargin{7}];
            obj.UseRatio = [obj.UseRatio; varargin{8}];
            obj.wUseRatio = [obj.wUseRatio; varargin{9}];
            obj.UniUseRatio = [obj.UniUseRatio; varargin{10}];
            obj.BiUseRatio = [obj.BiUseRatio; varargin{11}];
            obj.wUniUseRatio = [obj.wUniUseRatio; varargin{12}];
            obj.wBiUseRatio = [obj.wBiUseRatio; varargin{13}];
            obj.LUseCount = [obj.LUseCount; varargin{14}];
            obj.RUseCount = [obj.RUseCount; varargin{15}];
            obj.PercentNoMov = [obj.PercentNoMov; varargin{16}];
            obj.PercentMov = [obj.PercentMov; varargin{17}];
            obj.PercentSim = [obj.PercentSim; varargin{18}];
            obj.PercentDomUni = [obj.PercentDomUni; varargin{19}];
            obj.PercentNDUni = [obj.PercentNDUni; varargin{20}];
            obj.BiMagnitude = [obj.BiMagnitude; varargin{21}];
            obj.BiRatio = [obj.BiRatio; varargin{22}];
            obj.UniDom = [obj.UniDom; varargin{23}];
            obj.UniND = [obj.UniND; varargin{24}];
            obj.BiDom = [obj.BiDom; varargin{25}];
            obj.BiND = [obj.BiND; varargin{26}];
            obj.LWear = [obj.LWear; varargin{27}];
            obj.RWear = [obj.RWear; varargin{28}];
            obj.Sadeh_Sleep_Left = [obj.Sadeh_Sleep_Left; varargin{29}];
            obj.Sadeh_Sleep_Right = [obj.Sadeh_Sleep_Right; varargin{30}];
            obj.CK_Sleep_Left = [obj.CK_Sleep_Left; varargin{31}];
            obj.CK_Sleep_Right = [obj.CK_Sleep_Right; varargin{32}];
            obj.Galland_Sleep_Left = [obj.Galland_Sleep_Left; varargin{33}];
            obj.Galland_Sleep_Right = [obj.Galland_Sleep_Right; varargin{34}];
        end
        function clearsubject(obj,name)
            %CLEARSUBJECT - Delete subject from datalist

            % Find index of subject name
            idx = ismember(obj.Name,name);

            % clear data of subject
            obj.Name(idx) = [];
            obj.Age(idx) = [];
            obj.Visit(idx) = [];
            obj.Domhand(idx) = [];
            obj.Start(idx) = [];
            obj.End(idx) = [];
            obj.Period(idx) = [];
            obj.UseRatio(idx) = [];
            obj.UniUseRatio(idx) = [];
            obj.BiUseRatio(idx) = [];
            obj.wUseRatio(idx) = [];
            obj.wUniUseRatio(idx) = [];
            obj.wBiUseRatio(idx) = [];
            obj.LUseCount(idx) = [];
            obj.RUseCount(idx) = [];
            obj.PercentNoMov(idx) = [];
            obj.PercentMov(idx) = [];
            obj.PercentSim(idx) = [];
            obj.PercentDomUni(idx) = [];
            obj.PercentNDUni(idx) = [];
            obj.BiMagnitude(idx) = [];
            obj.BiRatio(idx) = [];
            obj.UniDom(idx) = [];
            obj.UniND(idx) = [];
            obj.BiDom(idx) = [];
            obj.BiND(idx) = [];
            obj.LWear(idx) = [];
            obj.RWear(idx) = [];
            obj.Sadeh_Sleep_Left(idx) = [];
            obj.Sadeh_Sleep_Right(idx) = [];
            obj.CK_Sleep_Left(idx) = [];
            obj.CK_Sleep_Right(idx) = [];
            obj.Galland_Sleep_Left(idx) = [];
            obj.Galland_Sleep_Right(idx) = [];
        end
        function writecsvfile(obj,settings,name)
            %WRITECSVFILE - Writes datalist to human readable CSV format

            % calculate percentage
            percentage = @(x) 100*sum(x)./length(x);

            % construct table for writing
            tw = [table(obj.Name,obj.Age,obj.Visit,obj.Domhand,obj.Start,obj.End,obj.Period,...
                obj.UseRatio,obj.UniUseRatio,obj.BiUseRatio,obj.wUseRatio,obj.wUniUseRatio,obj.wBiUseRatio,...
                obj.LUseCount,obj.RUseCount,obj.PercentNoMov,...
                obj.PercentMov,obj.PercentSim,obj.PercentDomUni,obj.PercentNDUni,'VariableNames',...
                {'Name','Age','Visit','Domhand','Start','End','Period',...
                'UseRatio','UniUseRatio','BiUseRatio','wUseRatio','wUniUseRatio','wBiUseRatio',...
                'LUseCount','RUseCount','PercentNoMov','PercentMov','PercentSim','PercentDomUni','PercentNDUni'}),...
                table(cellfun(@median,obj.BiMagnitude),...
                    cellfun(@max,obj.BiMagnitude),...
                    cellfun(@min,obj.BiMagnitude),...
                    cellfun(@median,obj.BiRatio),...
                    cellfun(@max,obj.BiRatio),...
                    cellfun(@min,obj.BiRatio),...
                    cellfun(percentage,obj.LWear),...
                    cellfun(percentage,obj.RWear),...
                    cellfun(percentage,obj.Sadeh_Sleep_Left),...
                    cellfun(@nnz,obj.Sadeh_Sleep_Left),...
                    cellfun(percentage,obj.Sadeh_Sleep_Right),...
                    cellfun(@nnz,obj.Sadeh_Sleep_Right),...
                    cellfun(percentage,obj.CK_Sleep_Left),...
                    cellfun(@nnz,obj.CK_Sleep_Left),...
                    cellfun(percentage,obj.CK_Sleep_Right),...
                    cellfun(@nnz,obj.CK_Sleep_Right),...
                    cellfun(percentage,obj.Galland_Sleep_Left),...
                    cellfun(@nnz,obj.Galland_Sleep_Left),...
                    cellfun(percentage,obj.Galland_Sleep_Right),...
                    cellfun(@nnz,obj.Galland_Sleep_Right),...
                    'VariableNames',{...
                    'MedianBilateralMagnitude','MaxBilateralMagnitude',...
                    'MinBilateralMagnitude','MedianMagnitudeRatio',...
                    'MaxMagnitudeRatio','MinMagnitudeRatio','LWearPercent',...
                    'RWearPercent','Percent_Sadeh_Sleep_Left','Total_Sadeh_Sleep_Left',...
                    'Percent_Sadeh_Sleep_Right','Total_Sadeh_Sleep_Right',...
                    'Percent_CK_Sleep_Left','Total_CK_Sleep_Left',...
                    'Percent_CK_Sleep_Right','Total_CK_Sleep_Right',...
                    'Percent_Galland_Sleep_Left','Total_Galland_Sleep_Left',...
                    'Percent_Galland_Sleep_Right','Total_Galland_Sleep_Right'})];

            % sort table
            tw = sortrows(tw,'Visit','ascend');
            tw = sortrows(tw,'Name','ascend');

             % create results directory if not exist
             if ~exist([settings.directory filesep 'results'],'dir')
                mkdir([settings.directory filesep 'results']);
             end

             % write table to csv
             writetable(tw,[settings.directory filesep 'results' filesep name '.csv']);
        end
    end

end
