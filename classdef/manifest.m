classdef manifest < handle
    %DATASET - class object defining list of subjects and visits in dataset
    %
    % Andrew Van, 05/24/17, vanandrew@wustl.edu
    
    properties
        subjectlist = table({},{},'VariableNames',{'Name','Visit'});
    end
    
    methods
        function addsubject(obj,name,visits)
            %ADDSUBJECT - adds subject to manifest
            
            obj.subjectlist(strcmp(obj.subjectlist.Name,name),:) = [];
            obj.subjectlist = [obj.subjectlist; table(name,visits,...
                'VariableNames',obj.subjectlist.Properties.VariableNames)];
            obj.subjectlist = sortrows(obj.subjectlist,'Name','ascend');
        end
        function list = comparelists(obj,obj2)
            %COMPARELISTS - compares manifest objects; returns unique names
            % from first input
            
            % get subjects not matching in name
            n_subs = obj.subjectlist(~ismember(obj.subjectlist.Name,obj2.subjectlist.Name),:);
            
            % get subjects matching in name 
            m_subs_obj = obj.subjectlist(ismember(obj.subjectlist.Name,obj2.subjectlist.Name),:);
            m_subs_obj2 = obj2.subjectlist(ismember(obj2.subjectlist.Name,obj.subjectlist.Name),:);
            
            % check if processed subject visits are same length; if not reprocess
            list = n_subs;
            for n=1:height(m_subs_obj)
                if length(m_subs_obj.Visit{n}) ~= length(m_subs_obj2.Visit{n})
                    list = [list; m_subs_obj(n,:)];  %#ok<AGROW>
                end
            end
            list = sortrows(list,'Name','ascend');
        end
    end
end

